import rospy
from tf import TransformListener
import tf
import geometry_msgs.msg
import sensor_msgs.msg
import copy

# Subscriber to /joint_states topic

rospy.init_node('ur10e_joints', anonymous=True)

FREQ_RATE = 10  # FREQ of publishing

class Publisher:
    def __init__(self, *args):

        self.tf_listener_ = TransformListener()
        self.pub = rospy.Publisher("ur10e/joint_states", sensor_msgs.msg.JointState, queue_size=10)
        self.sub_cb = "subscriber"
        self.joint_state = sensor_msgs.msg.JointState()

    def rev_arr(self, _arr):
        rarr = _arr[::-1]
        return rarr

    def js_callback(self, data):
        # Callback for the subscriber of /joint_states
        # Use a copy to avoid rewritin on the original
        js = copy.deepcopy(data)
        if js.name[0] != 'shoulder_pan_joint':
            # Set the order of links fom base to TCP, first shoulder pan, shoulder, lift, elbow etc
            # Instead of alphabetical as it is used as convention in 
            # Reverse first 3 data
            js.name[0:3] = self.rev_arr(js.name[0:3])
            # The followings are tuples, they cannot be assigned so we concatenate them with +
            js.position = self.rev_arr(js.position[0:3]) + js.position[3:6]
            js.velocity = self.rev_arr(js.velocity[0:3]) + js.velocity[3:6]
            js.effort = self.rev_arr(js.effort[0:3]) + js.effort[3:6]

        self.joint_state = js



    def pub_joint_states(self):
        #  t = self.tf_listener_.getLatestCommonTime("/base_link_inertia", "/wrist_3_link")
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():  # LOOP to give it time to recover the information asked
            try:
                t = rospy.Time.now()

                # Subscriber
                self.sub = rospy.Subscriber("/joint_states", sensor_msgs.msg.JointState, self.js_callback, queue_size=10)

                # print("joint_state from sub_cb?: \n", self.joint_state)

                # Publishing
                self.pub.publish(self.joint_state)

                rate.sleep()

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            


if __name__ == '__main__':
    try:
        joint_pub = Publisher()
        joint_pub.pub_joint_states()
    except rospy.ROSInterruptException:
        pass
