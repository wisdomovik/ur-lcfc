#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2021, LCFC
# All rights reserved.
#
# Author: Carlos Wilfrido PONCE QUIROGA

# Based first on the moveit tutorial interface by the Authors: Acorn Pooley, Mike Lautman

# ## 
# ##
# ## To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
# ## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
# ## and a `RobotCommander`_ class. More on these below. We also import `rospy`_ and some messages that we will use:
# ##

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit
import moveit_msgs.msg
import geometry_msgs.msg
import std_msgs.msg
import trajectory_msgs.msg

import numpy as np
from ur_lcfc.ur_lcfc_lib import Traj
# from ur_lcfc.urplot import plot_joint_trajectory
from ur_lcfc.helperJointTrajectoryGeneration import pchipJointTrajectoryGeneration
import ur_lcfc.urplot as urplot

from ur_lcfc.ur10_trajectory_client import TrajectoryClient

import ur_lcfc.trajectory as urtraj
import roboticstoolbox.tools.trajectory as tr

from tf.transformations import quaternion_from_euler
from tf import TransformListener

import tf

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

import PyKDL

degree = pi / 180
# ## END_SUB_TUTORIAL


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


def points_info(qq, *args, printinfo=True, plot=True, title=""):
    n = len(args)
    vv = np.zeros((qq.shape))
    aa = np.zeros((qq.shape))
    tt = np.linspace(0, 1, num=qq.shape[0])
    if printinfo:
        ttl1 = "> > " + title + " < <"
        print(ttl1)
        print('-> Cartesian plan: arrays')
        print("Positions:\n", qq)
        if n > 0:
            vv = args[0]
            aa = args[1]
            tt = args[2]
            print("Velocities:\n", vv)
            print("Accelerations:\n", aa)
        print("Pos/Vel/Acc shape:", qq.shape)
        print("tt shape   :", tt.shape)
        print("------------------------")
        ttl2 = "^ ^ " + title + " ^ ^"
        print(ttl2)
    if plot:
        urplot.plot_joint_trajectory(qq, vv, aa, tt)

def planned_points_info(_planned_points, printinfo=True, plot=True, title=""):

    qq, vv, aa, tt = parse_planned_points(_planned_points)
    if printinfo:
        ttl1 = "> > " + title + " < <"
        print(ttl1)
        print('-> Cartesian plan: arrays')
        print("Positions:\n", qq)
        print("Velocities:\n", vv)
        print("Accelerations:\n", aa)
        print("tt:\n", tt)
        print("Pos/Vel/Acc shape:", qq.shape)
        print("tt shape   :", tt.shape)
        print("------------------------")
        ttl2 = "^ ^ " + title + " ^ ^"
        print(ttl2)

    # Plot Moveit cartesian trajectory
    if plot:
        urplot.plot_joint_trajectory(qq, vv, aa, tt)


def parse_planned_points(plan):
    # Parse points messages from array 2 csv and to local variables planed_xxx
    # UR10e 6 axes
    # trajectory_msgs/JointTrajectoryPoint Message
    """
    float64[] positions
    float64[] velocities
    float64[] accelerations
    float64[] effort
    duration time_from_start
    -----
    Return
    Nx6 np.array
    position
    velocity
    accel
    """
    points_arr = plan
    # np.zeros is better than np.empty as later is randomly empty (depends of memory)
    planned_pos = np.zeros((1, 6))
    # print("posi 0", planned_pos[0, :])
    planned_vel = np.zeros((1, 6))
    planned_acc = np.zeros((1, 6))
    planned_time = np.zeros((1, 1))
    # planned_eff = np.zeros((1, 6))
    for _point in points_arr:
        # Seconds and nanoseconds
        ss = _point.time_from_start.secs
        ns = _point.time_from_start.nsecs
        # Seconds.nanoseconds
        t_secs = ss + ns * 10**(-9)

        # Parse time + POSITIONS to csv
        posi = [_point.positions[0], _point.positions[1], _point.positions[2],
                _point.positions[3], _point.positions[4], _point.positions[5]]

        # Parse time + VELOCITIES to csv
        veli = [_point.velocities[0], _point.velocities[1], _point.velocities[2],
                _point.velocities[3], _point.velocities[4], _point.velocities[5]]

        # Parse time + ACCELERATIONS to csv
        acci = [_point.accelerations[0], _point.accelerations[1], _point.accelerations[2],
                _point.accelerations[3], _point.accelerations[4], _point.accelerations[5]]

        # Parse time + EFFORT to csv
        if len(_point.effort) == 6:  # Check if it has the right length
            effi = [_point.effort[0], _point.effort[1], _point.effort[2],
                    _point.effort[3], _point.effort[4], _point.effort[5]]
        else:
            effi = [0, 0, 0, 0, 0, 0]

        # Store in GLOBAL variables the whole matrixes of Positions, Vel, Acc, Time
        planned_pos = np.append(planned_pos, np.array([posi]), axis=0)
        planned_vel = np.append(planned_vel, np.array([veli]), axis=0)
        planned_acc = np.append(planned_acc, np.array([acci]), axis=0)
        planned_time = np.append(planned_time, np.array([[t_secs]]), axis=0)

    # print("posi 0", planned_pos[0, :])
    # DELETE INIT ROW, because append with np.empty not necesarly is empty
    # Delete aswell 2nd row as it is always repeated the same values
    #  second row no more
    planned_pos = np.delete(planned_pos, [0], 0)  # , 1], 0)
    planned_vel = np.delete(planned_vel, [0], 0)  # , 1], 0)
    planned_acc = np.delete(planned_acc, [0], 0)  # , 1], 0)
    planned_time = np.delete(planned_time, [0], 0)  # , 1], 0)
    planned_time = planned_time.flatten()  # Make a one line array
    # print("posi 0", planned_pos[0, :])
    return planned_pos, planned_vel, planned_acc, planned_time


def test_trajectories():
    pass
    # THIS CODE WAS ACTUALLY IN THE PLACE WHERE test_trajectories() is called
    # print("times shape", tsi.shape)

    # # Plot joint trajectory using urplot.py
    # # qq, vv, aa, tt = parse_planned_points(_plan)
    # print("tt shape", tt.shape)
    # urplot.plot_joint_trajectory(qq, vv, aa, tt)

    # # Cubic interpolation given qq
    # qs, qds, qdds = urtraj.cubic_spline_interpolation(qq)
    # print(qs)
    # print("qs.shape", qs.shape)
    # urplot.plot_joint_trajectory(qs, qds, qdds, tsi)

    # # Spline interpolation given qq
    # qs, qds, qdds = urtraj.linear_spline_interpolation(qq, tsi)
    # urplot.plot_joint_trajectory(qs, qds, qdds, tsi)

    # # # # PETER CORKE INTERPO
    # via = qq
    # dt = tsi[1, 0]
    # print("dt: ", dt)  # 0.039
    # tac = tsi[1, 0]
    # tseg = tsi[1, 0]

    # qmax = [0.8, 0.8, 0.8, 0.8, 0.8, 0.8]
    # out = tr.mstraj(via, dt, tacc=dt / 2, qdmax=qmax)
    # ppc = out.s
    # vpc = out.sd
    # print(ppc)
    # print("ppc: ", ppc.shape)
    # print(vpc)
    # print("vpc: ", vpc.shape)
    # print("dt: ", dt)
    # urplot.plot_joint_trajectory(ppc, vpc, vpc, tsi)

    # dt = tsi[1, 0]
    # qmax = [1.5, 1.5, 1.5, 1.5, 1.5, 1.5]
    # qdf = [0, 0, 0, 0, 0, 0]
    # out = tr.mstraj(via, 0.01, tacc=0.01, qdmax=qmax, qdf=qdf)
    # ppc = out.s
    # vpc = out.sd
    # print(ppc)
    # print("ppc: ", ppc.shape)
    # print(vpc)
    # print("vpc: ", vpc.shape)
    # urplot.plot_joint_trajectory(ppc, vpc, vpc, [ppc.shape[0] * dt])

    # # Compute our own velocities from given Waypoints and Timestep
    # qdm = Traj.velocities_from_waypoints(ppc, dt)
    # print(qdm)
    # print("qdm: ", qdm.shape)


class MiscUR:
    # Class with several functions useful for the LCFC library and UR Robot Driver within ROS
    def x6_to_pose_msg(x6):
        # Creating/filling a Pose message from 1x6 vector
        # Vector with [ Position Orientation ]
        #             [x y z r p y]
        x6pose = geometry_msgs.msg.Pose()
        x6pose.position.x = x6[0]
        x6pose.position.y = x6[1]
        x6pose.position.z = x6[2]
        # # Pose Orientation
        # q = quaternion_from_euler(roll_angle, pitch_angle, yaw_angle)
        q = quaternion_from_euler(x6[3], x6[4], x6[5])
        x6pose.orientation.x = q[0]
        x6pose.orientation.y = q[1]
        x6pose.orientation.z = q[2]
        x6pose.orientation.w = q[3]

        return x6pose

    def x6np_mat_to_PoseArray(x6np_mat):
        # x6np is a NumPy matrix of x6xN
        _poseArray = []

        # IT IS NOT THE SAME POSEARRAY AS IN ROS
        # _poseArray = geometry_msgs.msg.PoseArray() 
        # _poseArray.header.frame_id = self._robot.get_planning_frame()
        # _poseArray.header.stamp = rospy.Time.now()
        lenc = x6np_mat.shape[1]  # NumPy columns size
        for i in range(lenc):
            _x6 = x6np_mat[:, i]
            # print(_x6)
            _x6pose = MiscUR.x6_to_pose_msg(_x6)
            _poseArray.append(_x6pose)
        return _poseArray

    def pos_2_joint_trajectory_point_array(pos, tt):
        # Fill an array of trajectory_msgs/JointTrajectoryPoint[] points
        # JUST POSITIONS TO USE IT WITH UR DRIVER TRAJECTORY CLIENT
        # UR10e 6 axes
        # trajectory_msgs/JointTrajectoryPoint Message
        #       to complete trajectory_msgs/JointTrajectory Message
        """
        Input
        ------
        pos: N x dof np array
        vel: N x dof np array
        acc: N x dof np array
        ------
        float64[] positions
        float64[] velocities
        float64[] accelerations
        float64[] effort
        duration time_from_start
                    ====
                    std_msgs/Header header
                    string[] joint_names
                    trajectory_msgs/JointTrajectoryPoint[] points
        -----
        Return
            trajectory_msgs/JointTrajectoryPoint[] points
        """

        points_arr = trajectory_msgs.msg.JointTrajectory().points
        point_msg = trajectory_msgs.msg.JointTrajectoryPoint()

        # wpose = move_group.get_current_pose().pose
        # waypoints.append(copy.deepcopy(wpose))
        # waypoints.append(targetPose)

        if isinstance(tt, np.ndarray):
            tt = tt.flatten()

        n = pos.shape[0]
        dof = pos.shape[1]
        for i in range(n):
            _point = copy.deepcopy(point_msg)

            ss = int(tt[i])  # retrieve the integer part
            ns = (tt[i] - ss) * 10**(9)
            ns = int(ns)
            _point.time_from_start.secs = ss
            _point.time_from_start.nsecs = ns

            _point.positions = pos[i, :].tolist()
            # _point.velocities = vel[i, :].tolist()
            # _point.accelerations = acc[i, :].tolist()

            # Append to array
            # trajectory_msgs/JointTrajectoryPoint[] points
            points_arr.append(_point)
        return points_arr

    def posvelacc_2_joint_trajectory_point_array(pos, vel, acc, tt):
        # Fill an array of trajectory_msgs/JointTrajectoryPoint[] points
        # UR10e 6 axes
        # trajectory_msgs/JointTrajectoryPoint Message
        #       to complete trajectory_msgs/JointTrajectory Message
        """
        Input
        ------
        pos: N x dof np array
        vel: N x dof np array
        acc: N x dof np array
        ------
        float64[] positions
        float64[] velocities
        float64[] accelerations
        float64[] effort
        duration time_from_start
                    ====
                    std_msgs/Header header
                    string[] joint_names
                    trajectory_msgs/JointTrajectoryPoint[] points
        -----
        Return
            trajectory_msgs/JointTrajectoryPoint[] points
        """

        points_arr = trajectory_msgs.msg.JointTrajectory().points
        point_msg = trajectory_msgs.msg.JointTrajectoryPoint()

        # wpose = move_group.get_current_pose().pose
        # waypoints.append(copy.deepcopy(wpose))
        # waypoints.append(targetPose)

        if isinstance(tt, np.ndarray):
            tt = tt.flatten()

        n = pos.shape[0]
        dof = pos.shape[1]
        for i in range(n):
            _point = copy.deepcopy(point_msg)

            ss = int(tt[i])  # retrieve the integer part
            ns = (tt[i] - ss) * 10**(9)
            ns = int(ns)
            _point.time_from_start.secs = ss
            _point.time_from_start.nsecs = ns

            _point.positions = pos[i, :].tolist()
            _point.velocities = vel[i, :].tolist()
            _point.accelerations = acc[i, :].tolist()

            # Append to array
            # trajectory_msgs/JointTrajectoryPoint[] points
            points_arr.append(_point)
        # print(tt)
        # print(tt.shape)
        # input("continue press enter: INSIDE posve;acc")
        return points_arr

class ur10ePublishers:
    def __init__(self, *args):

        self.tf_listener_ = TransformListener()
        self.pub = rospy.Publisher("ur10e/pose_publisher", geometry_msgs.msg.PoseStamped, queue_size=10)
        self.pub_mg = rospy.Publisher("ur10e/move_group_message", std_msgs.msg.String, queue_size=10)

    def pub_move_group_message(self, msg_string):
        _str = "" + msg_string
        """
        The publisher need some time to connect to the subscribers.
        The proper solution to your problem is to use
        pub.getNumSubscribers() and wait until that is > 0. Then publish.
        """

        rate = rospy.Rate(10)
        c1 = 1
        TIMEOUT = 1  # seconds
        while self.pub_mg.get_num_connections() < 1:
            c1 += 1
            if c1 / 10 >= TIMEOUT:
                    print("Publishing TIMOUT exeeded: ", TIMEOUT)
                    break
            rate.sleep()

        self.pub_mg.publish(_str)

    def pub_end_effector_pose(self):
        #  t = self.tf_listener_.getLatestCommonTime("/base_link_inertia", "/wrist_3_link")
        rate = rospy.Rate(10)
        c1 = 1
        TIMEOUT = 2  # seconds
        while not rospy.is_shutdown():  # LOOP to give it time to recover the information asked
            try:
                t = rospy.Time(0)
                (position, quat) = self.tf_listener_.lookupTransform("/base_link", "/wrist_3_link", t)
                # self.pub.publish(position, quat)

                # construct message
                pose_stamped = geometry_msgs.msg.PoseStamped()
                pose_stamped.header.stamp = rospy.Time.now()
                pose_stamped.header.frame_id = "base_link/wrist_3_link"

                pose_stamped.pose.position.x = position[0]
                pose_stamped.pose.position.y = position[1]
                pose_stamped.pose.position.z = position[2]

                pose_stamped.pose.orientation.x = quat[0]
                pose_stamped.pose.orientation.y = quat[1]
                pose_stamped.pose.orientation.z = quat[2]
                pose_stamped.pose.orientation.w = quat[3]
                self.pub.publish(pose_stamped)
                # print("Positioń: ", position)
                # print("Quaternion ", quat)
                # break # if there is no exception you can break :)
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                c1 += 1
                if c1 / 10 >= TIMEOUT:
                    print("Publishing TIMOUT exeeded: ", TIMEOUT)
                    break
                    return 0
                continue
            rate.sleep()

    def get_end_effector_pose(self):

        rate = rospy.Rate(10)
        c1 = 1
        TIMEOUT = 2  # seconds
        while not rospy.is_shutdown():  # LOOP to give it time to recover the information asked
            try:
                t = rospy.Time(0)
                (position, quat) = self.tf_listener_.lookupTransform("/base_link", "/wrist_3_link", t)
                # self.pub.publish(position, quat)

                # construct message
                pose_stamped = geometry_msgs.msg.PoseStamped()
                pose_stamped.header.stamp = rospy.Time.now()
                pose_stamped.header.frame_id = "base_link/wrist_3_link"

                pose_stamped.pose.position.x = position[0]
                pose_stamped.pose.position.y = position[1]
                pose_stamped.pose.position.z = position[2]

                pose_stamped.pose.orientation.x = quat[0]
                pose_stamped.pose.orientation.y = quat[1]
                pose_stamped.pose.orientation.z = quat[2]
                pose_stamped.pose.orientation.w = quat[3]
                # self.pub.publish(pose_stamped)
                return pose_stamped
                break # if there is no exception you can break :)
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                c1 += 1
                if c1 / 10 >= TIMEOUT:
                    print("Subscription TIMOUT exeeded: ", TIMEOUT)
                    break
                    return 0

                rate.sleep()
                continue


            rate.sleep()
        # return position, quat

class MoveGroupPythonInterface(object):
    """MoveGroupPythonInterface"""

    def __init__(self):
        super(MoveGroupPythonInterface, self).__init__()

        # ## BEGIN_SUB_TUTORIAL setup
        # ##
        # ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_ur10e", anonymous=True)

        # ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        # ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        # ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        # ## for getting, setting, and updating the robot's internal understanding of the
        # ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        # ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        # ## to a planning group (group of joints).  In this tutorial the group is the primary
        # ## arm joints in the UR10e robot, so we set the group's name to "manipulator".
        # ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        # ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # ## END_SUB_TUTORIAL

        # ## BEGIN_SUB_TUTORIAL basic_info
        # ##
        # ## Getting Basic Information
        # ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        # ## END_SUB_TUTORIAL

        # Setting max velocity scaling factor
        move_group.set_max_velocity_scaling_factor(0.5)
        print("============ set_max_velocity_scaling_factor")
        print("1.0")
        print("")

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        # ## BEGIN_SUB_TUTORIAL plan_to_joint_state
        # ##
        # ## Planning to a Joint Goal
        # ## ^^^^^^^^^^^^^^^^^^^^^^^^
        # ## The Panda's zero configuration is at a `singularity <https://www.quora.com/Robotics-What-is-meant-by-kinematic-singularity>`_, so the first
        # ## thing we want to do is move it to a slightly better configuration.
        # ## We use the constant `tau = 2*pi <https://en.wikipedia.org/wiki/Turn_(angle)#Tau_proposals>`_ for convenience:
        # We get the joint values from the group and change some of the values:
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -tau / 8
        joint_goal[1] = -tau / 4
        joint_goal[2] = tau / 6
        joint_goal[3] = tau / 8
        joint_goal[4] = 0
        joint_goal[5] = tau / 6  # 1/6 of a turn

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        # ## END_SUB_TUTORIAL

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def go_to_joint_state_list(self, joint_goal):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        # Confirm that joint goal has same length as current joint of the robot
        current_joint = move_group.get_current_joint_values()

        if len(joint_goal) != len(current_joint):
            print('Input goal has %d values, when it should have %d', len(joint_goal), len(current_joint))
            return False

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        # ## END_SUB_TUTORIAL

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def go_to_pose_goal(self, pose):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        # ## BEGIN_SUB_TUTORIAL plan_to_pose
        # ##
        # ## Planning to a Pose Goal
        # ## ^^^^^^^^^^^^^^^^^^^^^^^
        # ## We can plan a motion for this group to a desired pose for the
        # ## end-effector:
        pose_goal = pose
        # pose_goal.orientation.w = 1.0
        # pose_goal.position.x = -0.4
        # pose_goal.position.y = 0.3
        # pose_goal.position.z = 0.15

        move_group.set_pose_target(pose_goal)

        # ## Now, we call the planner to compute the plan and execute it.
        plan = move_group.go(wait=True)
        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        move_group.clear_pose_targets()

        # ## END_SUB_TUTORIAL

        # For testing:
        # Note that since this section of code will not be included in the tutorials
        # we use the class variable rather than the copied state variable
        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)

    def go_to_pose_goal1(self):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        # ## BEGIN_SUB_TUTORIAL plan_to_pose
        # ##
        # ## Planning to a Pose Goal
        # ## ^^^^^^^^^^^^^^^^^^^^^^^
        # ## We can plan a motion for this group to a desired pose for the
        # ## end-effector:
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 1.0
        pose_goal.position.x = -0.4
        pose_goal.position.y = 0.3
        pose_goal.position.z = 0.15

        move_group.set_pose_target(pose_goal)

        # ## Now, we call the planner to compute the plan and execute it.
        plan = move_group.go(wait=True)
        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        move_group.clear_pose_targets()

        # ## END_SUB_TUTORIAL

        # For testing:
        # Note that since this section of code will not be included in the tutorials
        # we use the class variable rather than the copied state variable
        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)

    def plan_cartesian_path(self, poseArray):
        # return value is a tuple:
        # 2) fraction of how much of the path was followed, 1) the actual RobotTrajectory
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        waypoints = copy.deepcopy(poseArray)

        # # Current Pose
        # wpose = move_group.get_current_pose().pose

        # # Append value in the first place of the waypoints
        # waypoints.insert(0, wpose)

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def plan_cartesian_pose(self, targetPose):
        # return value is a tuple: From Current Pose -> to target Pose
        # 2) fraction of how much of the path was followed, 1) the actual RobotTrajectory
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        waypoints.append(copy.deepcopy(wpose))
        waypoints.append(targetPose)

        # # Current Pose
        # wpose = move_group.get_current_pose().pose

        # # Append value in the first place of the waypoints
        # waypoints.insert(0, wpose)

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def plan_cartesian_path1(self, scale=1):
        # return value is a tuple:
        # 2) fraction of how much of the path was followed, 1) the actual RobotTrajectory
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        # ## BEGIN_SUB_TUTORIAL plan_cartesian_path
        # ##
        # ## Cartesian Paths
        # ## ^^^^^^^^^^^^^^^
        # ## You can plan a Cartesian path directly by specifying a list of waypoints
        # ## for the end-effector to go through. If executing  interactively in a
        # ## Python shell, set scale = 1.0.
        # ##
        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z -= scale * 0.1  # First move up (z)
        wpose.position.y += scale * 0.2  # and sideways (y)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y -= scale * 0.1  # Third move sideways (y)
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        print(waypoints)
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

        # ## END_SUB_TUTORIAL

    def display_trajectory(self, plan):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        # ## BEGIN_SUB_TUTORIAL display_trajectory
        # ##
        # ## Displaying a Trajectory
        # ## ^^^^^^^^^^^^^^^^^^^^^^^
        # ## You can ask RViz to visualize a plan (aka trajectory) for you. But the
        # ## group.plan() method does this automatically so this is not that useful
        # ## here (it just displays the same trajectory again):
        # ##
        # ## A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
        # ## We populate the trajectory_start with our current robot state to copy over
        # ## any AttachedCollisionObjects and add our plan to the trajectory.
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        display_trajectory_publisher.publish(display_trajectory)

        # ## END_SUB_TUTORIAL

    def execute_plan(self, plan):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        # ## Executing a Plan
        # ## ^^^^^^^^^^^^^^^^
        # ## Use execute if you would like the robot to follow
        # ## the plan that has already been computed:
        move_group.execute(plan, wait=True)

        # ## **Note:** The robot's current joint state must be within some tolerance of the
        # ## first waypoint in the `RobotTrajectory`_ or ``execute()`` will fail
        # ## END_SUB_TUTORIAL

class KDLur10e:

    def __init__(self, *q):
        super(KDLur10e, self).__init__()

        chain = PyKDL.Chain()

        # UR10e DH PARAM 
        #https://www.universal-robots.com/articles/ur/application-installation/dh-parameters-for-calculations-of-kinematics-and-dynamics/
        chain = PyKDL.Chain()

        # COMPUTE THE FRAMES FROM THE DH PARAM
        fr1 = PyKDL.Frame(PyKDL.Rotation.RPY(pi / 2, 0, 0.00), PyKDL.Vector(0.000000, 0.0000, 0.18070))
        fr2 = PyKDL.Frame(PyKDL.Rotation.RPY(0.0000, 0, 0.00), PyKDL.Vector(-0.61270, 0.0000, 0.00000))
        fr3 = PyKDL.Frame(PyKDL.Rotation.RPY(0.0000, 0, 0.00), PyKDL.Vector(-0.57155, 0.0000, 0.00000))
        fr4 = PyKDL.Frame(PyKDL.Rotation.RPY(pi / 2, 0, 0.00), PyKDL.Vector(0.000000, 0.0000, 0.17415))
        fr5 = PyKDL.Frame(PyKDL.Rotation.RPY(-pi / 2, 0, 0.0), PyKDL.Vector(0.000000, 0.0000, 0.11985))
        fr6 = PyKDL.Frame(PyKDL.Rotation.RPY(0.00000, 0.0, 0), PyKDL.Vector(0.000000, 0.0000, 0.11655))

        # ADD SEGMENTS TO CHAIN (LINKS WITH JOINTS AND FRAMES)
        chain.addSegment(PyKDL.Segment(PyKDL.Joint(PyKDL.Joint.RotZ), fr1))
        chain.addSegment(PyKDL.Segment(PyKDL.Joint(PyKDL.Joint.RotZ), fr2))
        chain.addSegment(PyKDL.Segment(PyKDL.Joint(PyKDL.Joint.RotZ), fr3))
        chain.addSegment(PyKDL.Segment(PyKDL.Joint(PyKDL.Joint.RotZ), fr4))
        chain.addSegment(PyKDL.Segment(PyKDL.Joint(PyKDL.Joint.RotZ), fr5))
        chain.addSegment(PyKDL.Segment(PyKDL.Joint(PyKDL.Joint.RotZ), fr6))

        # KINEMATIKS SOLVERS
        # FORWARD KINEMATIKS SOLVER
        fksolver = PyKDL.ChainFkSolverPos_recursive(chain)

        # INITIAL FRAME TR
        F_result = PyKDL.Frame()
        q = PyKDL.JntArray(chain.getNrOfJoints())  # init with zeros
        fksolver.JntToCart(q, F_result, chain.getNrOfJoints())
        endFrame = F_result
        # Convert endPOint Vector type to List array float]
        endPoint = [endFrame.p[0], endFrame.p[1], endFrame.p[2]]
        endM = endFrame.M

        # Misc variables
        self.chain = chain
        self.robot = chain
        self.fksolver = fksolver
        self.q = PyKDL.JntArray(self.chain.getNrOfJoints())
        self.frame = endFrame
        self.p = endPoint
        self.M = endM

    def fkpoint(self, qlist):
        # return p = [x y z]
        # Compute forward kinematics

        # Check if it is a np.ndarray
        if isinstance(qlist, np.ndarray):
            qlist = qlist.flatten()  # If it has 2 dimensions make it one
            if len(qlist) != self.chain.getNrOfJoints():
                return print("q array size is not correct (diff from 6): len=", len(qlist))

        q = self.JntArray(qlist)
        # self.q current is updated in JntArray
        p = self.get_endpoint()
        # TODO pass p as nativ list array
        return p

    def fkpoints(self, qq):
        # return pp = n x [x y z]
        # Compute the x y z points from qq matrix
        lenq = qq.shape[0]
        pp = np.zeros((1, 3))  # Init Points Matrix
        for i in range(lenq):
            _qq = qq[i, :]  # np array
            pi = self.fkpoint(_qq)  # pass a 
            pi = np.array([pi])  # Convert to np 2 D array
            pp = np.append(pp, np.array(pi), axis=0)
        pp = np.delete(pp, 0, 0)  # Delete np.zeros first row
        return pp

    def fkM(self, qlist):
        # return KDL T =
        # Compute forward kinematics
        q = self.JntArray(qlist)
        # self.q current is updated in JntArray
        p = self.get_endM()
        return p

    def JntArray(self, qlist):
        # Convert python array [a b c] to PyKDL.JntArray
        q = PyKDL.JntArray(self.chain.getNrOfJoints())
        LEN = len(qlist)
        for i in range(LEN):
            q[i] = qlist[i]
        self.q = q
        return self.q

    def print_endpoints(self, *q):
        # Print endpoints of each link at a given configuration of the UR
        if len(q) == 1:
            q = q[0]  # Overwrite q from value of index 0
            if type(q) == type(self.q):
                self.q = q
            elif len(q) == self.chain.getNrOfJoints():
                # Convert array [a b c] to PyKDL.JntArray
                self.q = self.JntArray(q)

        F_result = PyKDL.Frame()
        fksolver1 = self.fksolver
        for i in range(6):
            # Store in F_result the frame
            # i + 1 as the length as used in the KDL JntArray not
            # not from 0 to 5 but 1 to 6
            fksolver1.JntToCart(self.q, F_result, i + 1)
            print("Link ", i + 1, F_result.p)

    def get_endpoint(self):
        endFrame = PyKDL.Frame()
        fksolver = self.fksolver
        fksolver.JntToCart(self.q, endFrame, self.chain.getNrOfJoints())
        self.frame = endFrame
        # Convert endPOint Vector type to List array float]
        self.p = [endFrame.p[0], endFrame.p[1], endFrame.p[2]]
        self.M = endFrame.M
        return self.p

    def get_endM(self):
        endFrame = PyKDL.Frame()
        fksolver = self.fksolver
        fksolver.JntToCart(self.q, endFrame, self.chain.getNrOfJoints())
        self.frame = endFrame
        # Convert endPOint Vector type to List array float]
        self.p = [endFrame.p[0], endFrame.p[1], endFrame.p[2]]
        self.M = endFrame.M
        return endFrame.M

    def get_endframe(self):
        endFrame = PyKDL.Frame()
        fksolver = self.fksolver
        fksolver.JntToCart(self.q, endFrame, self.chain.getNrOfJoints())
        self.frame = endFrame
        # Convert endPOint Vector type to List array float]
        self.p = [endFrame.p[0], endFrame.p[1], endFrame.p[2]]
        self.M = endFrame.M
        return endFrame.M

    def test(self):
        AA = [-0.47266, -1.63115, -2.35073, -0.73007, 1.57162, -0.47227]
        UP = [0, -pi / 2, 0, -pi / 2, 0, 0]
        # for i in range(6):
        #     q1[i] = AA[i]
        # print("q1", q1)

        ur = self
        q1 = ur.JntArray(AA)
        q2 = ur.JntArray(UP)
        fksolver1 = ur.fksolver

        print(ur.fkpoint(AA))
        print(ur.fkpoint(UP))

        print(type(ur.p)) # TODO How to get a simple list
        print(ur.p)

def main():
    try:
        # Init ur KDL object for Forward kinematics and Inverse
        ur = KDLur10e()
        # ur.test()
        # input("ENTER ?")
        # -

        # =================================================
        # INITIALIZE NODE AND ALL
        moveint = MoveGroupPythonInterface()
        print("Loading up TrjectoryClient and searching for /controller_manager/switch_controller")
        driverClient = TrajectoryClient()
        # =================================================

        print(moveint.robot)

        moveint.move_group.set_max_velocity_scaling_factor(0.9)
        print('-> Setting set_max_velocity_scaling_factor(0.9) ')  # Check, it doesnt work?

        ur10e_pub = ur10ePublishers()

        # Just get the Pose from listener method, it does not 
        print('-> Getting current pose from tf:')
        pos1 = ur10e_pub.get_end_effector_pose()
        print(pos1)

        # Publish in topic /ur10e/move_group_message
        print('-> Publishing to topic : /ur10e/move_group_message')
        ur10e_pub.pub_move_group_message("move_group_python_interface_ur10e Started")
        print("... published!")

        input("==== Press enter to start")

        A = [0.4, -0.4, 0.25, 0, -180 * degree, 90 * degree]
        B = [0.5, +0.4, 0.46, 0, -180 * degree, 90 * degree]

        jointsA = [-0.47266, -1.63115, -2.35073, -0.73007, 1.57162, -0.47227]
        jointsB = [0.950187, -1.51715, -2.01193, -1.18331, 1.57080, 0.950187]

        x0 = np.array([A])  # Convert tu np matrix 1xN
        xf = np.array([B])
        x0t = np.transpose(x0)  # transpose Nx1
        x0f = np.concatenate((x0t, xf.transpose()), axis=1)  # matrix 2x6
        xo = -0.1
        R = 0.2
        h = R
        R2 = 0.5 * R
        R3 = 1 * R
        av11, b, c, d = Traj.arc_avoid_min(x0f, -0.1, R, 1.00 * h, R2, R3)
        av12, b, c, d = Traj.arc_avoid_min(x0f, -0.1, R, 0.75 * h, R2, R3)
        av13, b, c, d = Traj.arc_avoid_min(x0f, -0.1, R, 0.50 * h, R2, R3)
        av21, b, c, d = Traj.arc_avoid_min(x0f, 0.05, R, 1.00 * h, R2, R3)
        av22, b, c, d = Traj.arc_avoid_min(x0f, 0.05, R, 0.75 * h, R2, R3)
        av23, b, c, d = Traj.arc_avoid_min(x0f, 0.05, R, 0.50 * h, R2, R3)

        trajFuncStrings1 = ["========= Traj.arc_avoid_min(x0f, -0.1, R, 1.00 * h, R2, R3)",
                            "========= Traj.arc_avoid_min(x0f, -0.1, R, 0.70 * h, R2, R3)",
                            "========= Traj.arc_avoid_min(x0f, -0.1, R, 0.50 * h, R2, R3)"]

        trajFuncStrings2 = ["========= Traj.arc_avoid_min(x0f, 0.05, R, 1.00 * h, R2, R3)",
                            "========= Traj.arc_avoid_min(x0f, 0.05, R, 0.75 * h, R2, R3)",
                            "========= Traj.arc_avoid_min(x0f, 0.05, R, 0.50 * h, R2, R3)"]
        # print(av11)
        trajPoses_Avoid1 = []
        trajPoses_Avoid2 = []
        trajPoses_Avoid1.append(MiscUR.x6np_mat_to_PoseArray(av11))
        trajPoses_Avoid1.append(MiscUR.x6np_mat_to_PoseArray(av12))
        trajPoses_Avoid1.append(MiscUR.x6np_mat_to_PoseArray(av13))

        trajPoses_Avoid2.append(MiscUR.x6np_mat_to_PoseArray(av21))
        trajPoses_Avoid2.append(MiscUR.x6np_mat_to_PoseArray(av22))
        trajPoses_Avoid2.append(MiscUR.x6np_mat_to_PoseArray(av23))

        print("")
        print("----------------------------------------------------------")
        print("Welcome to the MoveIt MoveGroup Python Interface LCFC")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")
        input(
            "============ Press `Enter` to begin by SETTING UP the moveit_commander ..."
        )


        print("----------------------------------------------------------")
        print("Setting joints to AlphaPose:")
        print(jointsA)  # jointsA = [-0.47266, -1.63115, -2.35073, -0.73007, 1.57162, -0.47227]
        input(
            "============ Press `Enter` to EXECUTE a movement using a joint state goal..."
        )
        moveint.go_to_joint_state_list(jointsA)
        cjoints = moveint.move_group.get_current_joint_values()
        print('Current Joint values:')
        print(cjoints)

        """
                    print(trajPoses_Avoid1[0][0])
                    input("============ Press `Enter` to execute a movement using a pose goal ...")
                    moveint.go_to_pose_goal(trajPoses_Avoid1[0][0])
                    cjv3 = moveint.move_group.get_current_joint_values()
                    print('Current Joint values:')
                    print(cjv3)
        """
        poseB = MiscUR.x6_to_pose_msg(B)
        # = ----------------------------------------------------------------------------------------------------------------

        # ================== # ================== # ================== # ================== #
        # ========================   LINEAR 0-0  A to B         =========================== #

        input("============ Press `Enter` to PLAN and display a Cartesian A - B traj...")
        poseB = MiscUR.x6_to_pose_msg(B)
        cartesian_plan, fraction = moveint.plan_cartesian_pose(poseB)

        # # # ===== GET CARTESIANS POINTS FROM Q PLAN
        _planned_points = cartesian_plan.joint_trajectory.points
        qq, vv, aa, ttm = parse_planned_points(_planned_points)
        planned_points_info(_planned_points, plot=False, title="MoveIt Cartesian Points")
        print('-> Fraction (%% complted plan): %.2f ' % fraction)
        print('-> Final expected pose: \n', poseB)
        print('cartesian_plan length', len(_planned_points))
        print('parsed_plan length', len(ttm))
        print("ttm \n", ttm)

        # # = = ----------------------------------------------------------------
        '''
                # #  = = = . . . CHANGE SPEEED
                # Get the Forward Kinematics of qq waypoints
                pp = ur.fkpoints(qq)

                # # #
                # # # ===== COMPUTE NEW TIMES GIVEN CARTESIAN SPEED
                speed = 0.25  # m/s
                tts = Traj.times_at_cartesian_points(pp, speed)
                print("Total time: \n", tts)

                # # #
                # # # ===== ARANGE NEW TIMES WITH LESS/MORE DATA POINTS
                dt = tts[1] * 1.0  # Increase the factor to reduce the number of points
                tts2 = np.arange(0, tts[-1], dt)
                tts2 = np.hstack((tts2, tts2[-1] + dt))  # Add last dt to make finish at the same time as tts
                print("tts2 new time arange: \n", tts2)

                input("continue press enter:")

                # # #
                # # # ===== PCHIP JOINT TRAJECTORY GENERATION     (x,    y,   xx)
                qs, qds, qdds, tt = pchipJointTrajectoryGeneration(tts, qq, tts2)
                # print("Velocity \n", qds)
        '''
        #    ---------------------------------------------------------------- = = #
        #                                                                         #
        #              PCHIP JOINT TRAJECTORY GENERATION ======================== #
        #                                                                         #
        #    ---------------------------------------------------------------- = = #
        # # # ===== PCHIP JOINT TRAJECTORY GENERATION     (x,    y,   xx)
        qs, qds, qdds, ttchip = pchipJointTrajectoryGeneration(ttm, qq, ttm)
        print("ttm \n", ttm)

        # print("Velocity \n", qds)
        # # #
        # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
        pchip_points = MiscUR.posvelacc_2_joint_trajectory_point_array(qs, qds, qdds, ttchip)
        # Copy original moveit cartesian_plan
        cartesian_plan_pchip = copy.deepcopy(cartesian_plan)
        # COPY THE NEW CARTESIAN POINTS
        cartesian_plan_pchip.joint_trajectory.points = pchip_points
        planned_points_info(pchip_points, plot=False, title="PCHIP cartesian points")

        # Comparison of linear with same shapes betwen moveit and pchip for corroboration
        qqc = qq - qs
        vvc = vv - qds
        aac = aa - qdds
        ttc = ttm - ttchip
        points_info(qqc, vvc, aac, ttc, plot=False, title="MOVIT pts - PCHIP points")

        # = = ----------------------------------------------------------------
        # = =
        # ============ EXECUTE MOVIT ORIGINAL PATH
        # = =
        # = = ----------------------------------------------------------------
        input("============ Press `Enter` to execute MOVEIT saved path ...")
        moveint.execute_plan(cartesian_plan)
        cpose = moveint.move_group.get_current_pose().pose
        print('-> Current Pose: \n', cpose)
        cjoints = moveint.move_group.get_current_joint_values()
        print('-> Current Joint values:\n', cjoints)

        # # # # # # # # # #      /|
        #   RETURN TO A   #  A  < | ==============
        # # # # # # # # # #      \|
        input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
        print("-> Setting joints to AlphaPose:")
        print(jointsA)
        moveint.go_to_joint_state_list(jointsA)
        cjoints = moveint.move_group.get_current_joint_values()
        print('-> Current Joint values:\n', cjoints)
        cpose = moveint.move_group.get_current_pose().pose
        print('-> Current Pose: \n', cpose)

        #    ---------------------------------------------------------------- = = #
        #                                                                         #
        #              PCHIP JOINT TRAJECTORY GENERATION 2 ====================== #
        #                              CHANGING SPEED !!                          #
        #    ---------------------------------------------------------------- = = #
        speeds = [0.149, 0.2, 0.25]  # m/s
        stt = [0.045, 0.06, 0.075]       # s  settling time rising time?

        for i in range(len(speeds)):

            # #  = = = . . . CHANGE SPEEED
            # Get the Forward Kinematics of qq waypoints
            pp = ur.fkpoints(qq)

            # # # ===== PCHIP JOINT TRAJECTORY GENERATION -1 for SPEED    (x,    y,   xx)
            dt = 0.015
            tforSpeed = np.arange(0, ttm[-1], dt)
            print("tforSpeed \n", tforSpeed)
            tforSpeed = np.hstack((tforSpeed, tforSpeed[-1] + dt))  # Add last dt to make finish at the same time as tts
            print("tforSpeed \n", tforSpeed)
            print("ttchip \n", ttchip)
            qsX, qdsX, qddsX, ttX = pchipJointTrajectoryGeneration(ttchip, qs, tforSpeed)

            # #  = = = . . . CHANGE SPEEED
            # Get the Forward Kinematics of qq waypoints
            ppX = ur.fkpoints(qsX)

            # # #
            # # # ===== COMPUTE NEW TIMES GIVEN CARTESIAN SPEED
            speed = 0.149  # m/s
            tts = Traj.times_at_cartesian_points(ppX, speeds[i], ttX, ST_WINDOW=stt[i])
            print("Total time with MIDDLE PHASE SPEED: \n", tts)

            # # #
            # # # ===== ARANGE NEW TIMES WITH LESS/MORE DATA POINTS
            dt = 0.02 * 1.0  # Increase the factor to reduce the number of points
            tts2 = np.arange(0, tts[-1], dt)
            tts2 = np.hstack((tts2, tts2[-1] + dt))  # Add last dt to make finish at the same time as tts
            print("tts2 new time arange: \n", tts2)
            print("tt len", len(tts2))
            input("continue press enter:")

            # # # ===== PCHIP JOINT TRAJECTORY GENERATION     (x,    y,   xx)
            qs2, qds2, qdds2, ttchip2 = pchipJointTrajectoryGeneration(tts, qsX, tts)
            # print("Velocity \n", qds)
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pchip_points2 = MiscUR.posvelacc_2_joint_trajectory_point_array(qs2, qds2, qdds2, ttchip2)
            # Copy original moveit cartesian_plan
            cartesian_plan_pchip2 = copy.deepcopy(cartesian_plan)
            # COPY THE NEW CARTESIAN POINTS
            cartesian_plan_pchip2.joint_trajectory.points = pchip_points2
            planned_points_info(pchip_points2, plot=True, title="PCHIP 2 SPEED cartesian points")

            # = = ----------------------------------------------------------------
            # = =                    SPEED                 PCHIP2
            # ============ EXECUTE UR DRIVER - only POSITION + TIME given
            # = =
            # = = ----------------------------------------------------------------
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pos_points2 = MiscUR.pos_2_joint_trajectory_point_array(qs2, ttchip2)
            print(pos_points2)
            print("movit_points: ^")
            print("ttchip2: ", ttchip2)
            print("ttm: ", ttm)

            print("                 SPEED OF ", speeds[i])
            input("cont press enter: to send PCHIP SPEEED! with only POS plan from A->B\n\
                  # = = ----------------------------------------------------------------\n\
                  # = =                    SPEED                 PCHIP2\n\
                  # ============ EXECUTE UR DRIVER - only POSITION + TIME given\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            # SIMULATION
            moveint.execute_plan(cartesian_plan_pchip2)
            # DRIVER CLIENT
            driverClient.send_joint_trajectory_plan(pos_points2)
            print("-^ LINEAR PATH EXECUTED WITH SPEED")

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)

        # = ----------------------------------------------------------------------------------------------------------------

        # ================== # ================== # ================== # ================== #
        # ========================    AVOID  1-0  1-1  1-2  =========================== #

        for i in range(len(trajPoses_Avoid1)):
            print(trajFuncStrings1[i])
            input("========= Press `Enter` to plan and display a Cartesian AVOID 1 [%d]" % i)
            cartesian_plan, fraction = moveint.plan_cartesian_path(trajPoses_Avoid1[i])

            print('-> Cartesian plan: \n', cartesian_plan)
            print('-> Final expected pose: \n', trajPoses_Avoid1[0][-1])
            print('- - -> Fraction (%%Path followed) : %.2f ' % fraction)

            # = = ----------------------------------------------------------------
            # # #
            # # # ===== GET CARTESIANS POINTS FROM Q PLAN
            _planned_points = cartesian_plan.joint_trajectory.points
            qq, vv, aa, ttm = parse_planned_points(_planned_points)
            planned_points_info(_planned_points, plot=False, title="MoveIt Cartesian Points")
            print('-> Fraction (%% complted plan): %.2f ' % fraction)
            print('-> Final expected pose: \n', poseB)
            print('cartesian_plan length', len(_planned_points))
            print('parsed_plan length', len(ttm))

            # # = = ----------------------------------------------------------------
            #    ---------------------------------------------------------------- = = #
            #                                                                         #
            #              PCHIP JOINT TRAJECTORY GENERATION ======================== #
            #                                                                         #
            #    ---------------------------------------------------------------- = = #
            # # # ===== PCHIP JOINT TRAJECTORY GENERATION     (x,    y,   xx)
            qs, qds, qdds, ttchip = pchipJointTrajectoryGeneration(ttm, qq, ttm)
            # print("Velocity \n", qds)
            # # #
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pchip_points = MiscUR.posvelacc_2_joint_trajectory_point_array(qs, qds, qdds, ttchip)
            # Copy original moveit cartesian_plan
            cartesian_plan_pchip = copy.deepcopy(cartesian_plan)
            # COPY THE NEW CARTESIAN POINTS
            cartesian_plan_pchip.joint_trajectory.points = pchip_points
            planned_points_info(pchip_points, plot=False, title="PCHIP cartesian points")

            # # Comparison of linear with same shapes betwen moveit and pchip for corroboration
            # qqc = qq - qs
            # vvc = vv - qds
            # aac = aa - qdds
            # ttc = ttm - ttchip
            # points_info(qqc, vvc, aac, ttc, plot=True, title="MOVIT pts - PCHIP points")

            #    ---------------------------------------------------------------- = = #
            #                                                                         #
            #              PCHIP JOINT TRAJECTORY GENERATION 2 ====================== #
            #                              CHANGING SPEED !!                          #
            #    ---------------------------------------------------------------- = = #
            # #  = = = . . . CHANGE SPEEED
            # Get the Forward Kinematics of qq waypoints
            pp = ur.fkpoints(qq)

            # # #
            # # # ===== COMPUTE NEW TIMES GIVEN CARTESIAN SPEED
            speed = 0.25  # m/s
            tts = Traj.times_at_cartesian_points(pp, speed, ttm, ST_WINDOW=0.3)
            print("Total time with MIDDLE PHASE SPEED: \n", tts)

            # # #
            # # # ===== ARANGE NEW TIMES WITH LESS/MORE DATA POINTS
            dt = 0.02 * 1.0  # Increase the factor to reduce the number of points
            tts2 = np.arange(0, tts[-1], dt)
            tts2 = np.hstack((tts2, tts2[-1] + dt))  # Add last dt to make finish at the same time as tts
            print("tts2 new time arange: \n", tts2)
            print("tt len", len(tts2))
            input("continue press enter:")

            # # # ===== PCHIP JOINT TRAJECTORY GENERATION     (x,    y,   xx)
            qs2, qds2, qdds2, ttchip2 = pchipJointTrajectoryGeneration(tts, qq, tts)
            # print("Velocity \n", qds)
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pchip_points2 = MiscUR.posvelacc_2_joint_trajectory_point_array(qs2, qds2, qdds2, ttchip2)
            # Copy original moveit cartesian_plan
            cartesian_plan_pchip2 = copy.deepcopy(cartesian_plan)
            # COPY THE NEW CARTESIAN POINTS
            cartesian_plan_pchip2.joint_trajectory.points = pchip_points2
            planned_points_info(pchip_points2, plot=False, title="PCHIP 2 SPEED cartesian points")

            # = = ----------------------------------------------------------------
            # = =
            # ============ EXECUTE MOVIT ORIGINAL PATH
            # = =
            # = = ----------------------------------------------------------------
            input("============ Press `Enter` to execute MOVEIT saved path ...\n\
                  # = = ----------------------------------------------------------------\n\
                  # = =\n\
                  # ============ EXECUTE MOVIT ORIGINAL PATH\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            moveint.execute_plan(cartesian_plan)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)

            '''
                # # = = ----------------------------------------------------------------
                # # = =
                # # ============ EXECUTE MOVIT with PCHIP
                # # = =
                # # = = ----------------------------------------------------------------

                # # # ============ EXECUTE  Path with Pchip interpolation
                # # input("============ Press `Enter` to execute with PCHIP Interp ...")
                # # moveint.execute_plan(cartesian_plan_pchip)
                # # cpose = moveint.move_group.get_current_pose().pose
                # # print('-> Current Pose: \n', cpose)
                # # cjoints = moveint.move_group.get_current_joint_values()
                # # print('-> Current Joint values:\n', cjoints)

                # # # # # # # # # # # #      /|
                # # #   RETURN TO A   #  A  < | ==============
                # # # # # # # # # # # #       |
                # # input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
                # # print("-> Setting joints to AlphaPose:")
                # # print(jointsA)
                # # moveint.go_to_joint_state_list(jointsA)
                # # cjoints = moveint.move_group.get_current_joint_values()
                # # print('-> Current Joint values:\n', cjoints)
                # # cpose = moveint.move_group.get_current_pose().pose
                # # print('-> Current Pose: \n', cpose)
            '''

            # = ----------------------------------------------------------------------------------------------------------------
            # = ----------------------------------------------------------------------------------------------------------------
            # = ----------------------------------------------------------------------------------------------------------------
            # ======        TEST  UR ROS DRIVER TRJECTORY CLIENT

            print("TrjectoryClient ALREADY UP and searching for /controller_manager/switch_controller")

            # = = ----------------------------------------------------------------
            # = =                                       PCHIP
            # ============ EXECUTE UR DRIVER - only POSITION + TIME given
            # = =
            # = = ----------------------------------------------------------------
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pos_points = MiscUR.pos_2_joint_trajectory_point_array(qs, ttchip)
            print(pos_points)
            print("movit_points: ^")
            print("ttchip: ", ttchip)
            print("ttm: ", ttm)

            input("cont press enter: to send PCHIP with only POS plan from A->B\n\
                  # = = ----------------------------------------------------------------\n\
                  # = =                                        PCHIP\n\
                  # ============ EXECUTE UR DRIVER - only POSITION + TIME given\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            driverClient.send_joint_trajectory_plan(pos_points)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)

            # = = ----------------------------------------------------------------
            # = =                                       MOVIT
            # ============ EXECUTE UR DRIVER - only POSITION + TIME given
            # = =
            # = = ----------------------------------------------------------------
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            # movit_points = MiscUR.posvelacc_2_joint_trajectory_point_array(qq, vv, aa, ttm)
            movit_points = MiscUR.pos_2_joint_trajectory_point_array(qq, ttm)
            print(movit_points)
            print("movit_points: ^")
            print("qs -> pchip\n", qs)
            print("qs.shape-> pchip\n", qs.shape)
            print("qq -> movit\n", qq)
            print("qq.shape -> movit\n", qq.shape)

            input("continue press enter: to try send MOVIT POINTS in driver just POS plan A->B \n\
                  # = = ----------------------------------------------------------------\n\
                  # = =                                       MOVIT\n\
                  # ============ EXECUTE UR DRIVER - only POSITION + TIME given\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            driverClient.send_joint_trajectory_plan(movit_points)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)

            # = = ----------------------------------------------------------------
            # = =                    SPEED                 PCHIP2
            # ============ EXECUTE UR DRIVER - only POSITION + TIME given
            # = =
            # = = ----------------------------------------------------------------
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pos_points2 = MiscUR.pos_2_joint_trajectory_point_array(qs2, ttchip2)
            print(pos_points2)
            print("movit_points: ^")
            print("ttchip2: ", ttchip2)
            print("ttm: ", ttm)

            input("cont press enter: to send PCHIP SPEEED! with only POS plan from A->B\n\
                  # = = ----------------------------------------------------------------\n\
                  # = =                    SPEED                 PCHIP2\n\
                  # ============ EXECUTE UR DRIVER - only POSITION + TIME given\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            driverClient.send_joint_trajectory_plan(pos_points2)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)


        # = ----------------------------------------------------------------------------------------------------------------

        # ================== # ================== # ================== # ================== #
        # ========================    AVOID  2-0  2-1  2-2    =========================== #

        for i in range(len(trajPoses_Avoid2)):
            print(trajFuncStrings2[i])
            input("========= Press `Enter` to plan and display a Cartesian AVOID 2 [%d]" % i)
            cartesian_plan, fraction = moveint.plan_cartesian_path(trajPoses_Avoid2[i])

            print('-> Cartesian plan: \n', cartesian_plan)
            print('-> Final expected pose: \n', trajPoses_Avoid1[0][-1])
            print('- - -> Fraction (%%Path followed) : %.2f ' % fraction)

            # = = ----------------------------------------------------------------
            # # #
            # # # ===== GET CARTESIANS POINTS FROM Q PLAN
            _planned_points = cartesian_plan.joint_trajectory.points
            qq, vv, aa, ttm = parse_planned_points(_planned_points)
            planned_points_info(_planned_points, plot=True, title="MoveIt Cartesian Points")
            print('-> Fraction (%% complted plan): %.2f ' % fraction)
            print('-> Final expected pose: \n', poseB)
            print('cartesian_plan length', len(_planned_points))
            print('parsed_plan length', len(ttm))

            # # = = ----------------------------------------------------------------
            #    ---------------------------------------------------------------- = = #
            #                                                                         #
            #              PCHIP JOINT TRAJECTORY GENERATION ======================== #
            #                                                                         #
            #    ---------------------------------------------------------------- = = #
            # # # ===== PCHIP JOINT TRAJECTORY GENERATION     (x,    y,   xx)
            qs, qds, qdds, ttchip = pchipJointTrajectoryGeneration(ttm, qq, ttm)
            # print("Velocity \n", qds)
            # # #
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pchip_points = MiscUR.posvelacc_2_joint_trajectory_point_array(qs, qds, qdds, ttchip)
            # Copy original moveit cartesian_plan
            cartesian_plan_pchip = copy.deepcopy(cartesian_plan)
            # COPY THE NEW CARTESIAN POINTS
            cartesian_plan_pchip.joint_trajectory.points = pchip_points
            planned_points_info(pchip_points, plot=True, title="PCHIP cartesian points")

            # Comparison of linear with same shapes betwen moveit and pchip for corroboration
            qqc = qq - qs
            vvc = vv - qds
            aac = aa - qdds
            ttc = ttm - ttchip
            points_info(qqc, vvc, aac, ttc, plot=True, title="MOVIT pts - PCHIP points")
            #    ---------------------------------------------------------------- = = #
            #                                                                         #
            #              PCHIP JOINT TRAJECTORY GENERATION 2 ====================== #
            #                              CHANGING SPEED !!                          #
            #    ---------------------------------------------------------------- = = #
            # #  = = = . . . CHANGE SPEEED
            # Get the Forward Kinematics of qq waypoints
            pp = ur.fkpoints(qq)

            # # #
            # # # ===== COMPUTE NEW TIMES GIVEN CARTESIAN SPEED
            speed = 0.149  # m/s
            tts = Traj.times_at_cartesian_points(pp, speed, ttm, ST_WINDOW=0.3)
            print("Total time with MIDDLE PHASE SPEED: \n", tts)

            # # #
            # # # ===== ARANGE NEW TIMES WITH LESS/MORE DATA POINTS
            dt = 0.02 * 1.0  # Increase the factor to reduce the number of points
            tts2 = np.arange(0, tts[-1], dt)
            tts2 = np.hstack((tts2, tts2[-1] + dt))  # Add last dt to make finish at the same time as tts
            print("tts2 new time arange: \n", tts2)
            print("tt len", len(tts2))
            input("continue press enter:")

            # # # ===== PCHIP JOINT TRAJECTORY GENERATION     (x,    y,   xx)
            qs2, qds2, qdds2, ttchip2 = pchipJointTrajectoryGeneration(tts, qq, tts)
            # print("Velocity \n", qds)
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pchip_points2 = MiscUR.posvelacc_2_joint_trajectory_point_array(qs2, qds2, qdds2, ttchip2)
            # Copy original moveit cartesian_plan
            cartesian_plan_pchip2 = copy.deepcopy(cartesian_plan)
            # COPY THE NEW CARTESIAN POINTS
            cartesian_plan_pchip2.joint_trajectory.points = pchip_points2
            planned_points_info(pchip_points2, plot=True, title="PCHIP 2 SPEED cartesian points")

            # = = ----------------------------------------------------------------
            # = =
            # ============ EXECUTE MOVIT ORIGINAL PATH
            # = =
            # = = ----------------------------------------------------------------
            input("============ Press `Enter` to execute MOVEIT saved path ...\n\
                  # = = ----------------------------------------------------------------\n\
                  # = =\n\
                  # ============ EXECUTE MOVIT ORIGINAL PATH\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            moveint.execute_plan(cartesian_plan)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)

            '''
                # # = = ----------------------------------------------------------------
                # # = =
                # # ============ EXECUTE MOVIT with PCHIP
                # # = =
                # # = = ----------------------------------------------------------------

                # # # ============ EXECUTE  Path with Pchip interpolation
                # # input("============ Press `Enter` to execute with PCHIP Interp ...")
                # # moveint.execute_plan(cartesian_plan_pchip)
                # # cpose = moveint.move_group.get_current_pose().pose
                # # print('-> Current Pose: \n', cpose)
                # # cjoints = moveint.move_group.get_current_joint_values()
                # # print('-> Current Joint values:\n', cjoints)

                # # # # # # # # # # # #      /|
                # # #   RETURN TO A   #  A  < | ==============
                # # # # # # # # # # # #       |
                # # input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
                # # print("-> Setting joints to AlphaPose:")
                # # print(jointsA)
                # # moveint.go_to_joint_state_list(jointsA)
                # # cjoints = moveint.move_group.get_current_joint_values()
                # # print('-> Current Joint values:\n', cjoints)
                # # cpose = moveint.move_group.get_current_pose().pose
                # # print('-> Current Pose: \n', cpose)
            '''

            # = ----------------------------------------------------------------------------------------------------------------
            # = ----------------------------------------------------------------------------------------------------------------
            # = ----------------------------------------------------------------------------------------------------------------
            # ======        TEST  UR ROS DRIVER TRJECTORY CLIENT

            print("TrjectoryClient ALREADY UP and searching for /controller_manager/switch_controller")

            # = = ----------------------------------------------------------------
            # = =                                       PCHIP
            # ============ EXECUTE UR DRIVER - only POSITION + TIME given
            # = =
            # = = ----------------------------------------------------------------
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pos_points = MiscUR.pos_2_joint_trajectory_point_array(qs, ttchip)
            print(pos_points)
            print("movit_points: ^")
            print("ttchip: ", ttchip)
            print("ttm: ", ttm)

            input("continue press enter: to try send PCHIP just POS plan A->B\n\
                  # = = ----------------------------------------------------------------\n\
                  # = =                    SPEED                 PCHIP2\n\
                  # ============ EXECUTE UR DRIVER - only POSITION + TIME given\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            driverClient.send_joint_trajectory_plan(pos_points)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)

            # = = ----------------------------------------------------------------
            # = =                                       MOVIT
            # ============ EXECUTE UR DRIVER - only POSITION + TIME given
            # = =
            # = = ----------------------------------------------------------------
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            # movit_points = MiscUR.posvelacc_2_joint_trajectory_point_array(qq, vv, aa, ttm)
            movit_points = MiscUR.pos_2_joint_trajectory_point_array(qq, ttm)
            print(movit_points)
            print("movit_points: ^")
            print("qs -> pchip\n", qs)
            print("qs.shape-> pchip\n", qs.shape)
            print("qq -> movit\n", qq)
            print("qq.shape -> movit\n", qq.shape)

            input("continue press enter: to try send MOVIT POINTS in driver just POS plan A->B \n\
                  # = = ----------------------------------------------------------------\n\
                  # = =                                       MOVIT\n\
                  # ============ EXECUTE UR DRIVER - only POSITION + TIME given\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            driverClient.send_joint_trajectory_plan(movit_points)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)

            # = = ----------------------------------------------------------------
            # = =                    SPEED                 PCHIP2
            # ============ EXECUTE UR DRIVER - only POSITION + TIME given
            # = =
            # = = ----------------------------------------------------------------
            # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
            pos_points2 = MiscUR.pos_2_joint_trajectory_point_array(qs2, ttchip2)
            print(pos_points2)
            print("movit_points: ^")
            print("ttchip2: ", ttchip2)
            print("ttm: ", ttm)

            input("continue press enter: to try send PCHIP SPEEED! just POS plan A->B\n\
                  # = = ----------------------------------------------------------------\n\
                  # = =                    SPEED                 PCHIP2\n\
                  # ============ EXECUTE UR DRIVER - only POSITION + TIME given\n\
                  # = =\n\
                  # = = ----------------------------------------------------------------")
            driverClient.send_joint_trajectory_plan(pos_points2)

            # # # # # # # # # #      /|
            #   RETURN TO A   #  A  < | ==============
            # # # # # # # # # #      \|
            input("============ Press `Enter` to PLAN and EXECUTE a RETURN [joint state goal]...")
            print("-> Setting joints to AlphaPose:")
            print(jointsA)
            moveint.go_to_joint_state_list(jointsA)
            cjoints = moveint.move_group.get_current_joint_values()
            print('-> Current Joint values:\n', cjoints)
            cpose = moveint.move_group.get_current_pose().pose
            print('-> Current Pose: \n', cpose)








        # input(
        #     "============ Press `Enter` to execute a movement using a joint state goal ..."
        # )
        # tutorial.go_to_joint_state()

        # input("============ Press `Enter` to execute a movement using a pose goal ...")
        # tutorial.go_to_pose_goal1()

        # input("============ Press `Enter` to plan and display a Cartesian path ...")
        # cartesian_plan, fraction = tutorial.plan_cartesian_path()

        # input(
        #     "============ Press `Enter` to display a saved trajectory (this will replay the Cartesian path)  ..."
        # )
        # tutorial.display_trajectory(cartesian_plan)

        # input("============ Press `Enter` to execute a saved path ...")
        # tutorial.execute_plan(cartesian_plan)

        print("============ Python tutorial demo complete!")
    except rospy.ROSInterruptException:
        print('move_group_python_interface_ur10e Finished')
        return
    except KeyboardInterrupt:
        ur10e_pub.pub_move_group_message('move_group_python_interface_ur10e Finished')
        print('move_group_python_interface_ur10e Finished')
        return


if __name__ == "__main__":
    main()

# ## BEGIN_TUTORIAL
# ## .. _moveit_commander:
# ##    http://docs.ros.org/noetic/api/moveit_commander/html/namespacemoveit__commander.html
# ##
# ## .. _MoveGroupCommander:
# ##    http://docs.ros.org/noetic/api/moveit_commander/html/classmoveit__commander_1_1move__group_1_1MoveGroupCommander.html
# ##
# ## .. _RobotCommander:
# ##    http://docs.ros.org/noetic/api/moveit_commander/html/classmoveit__commander_1_1robot_1_1RobotCommander.html
# ##
# ## .. _PlanningSceneInterface:
# ##    http://docs.ros.org/noetic/api/moveit_commander/html/classmoveit__commander_1_1planning__scene__interface_1_1PlanningSceneInterface.html
# ##
# ## .. _DisplayTrajectory:
# ##    http://docs.ros.org/noetic/api/moveit_msgs/html/msg/DisplayTrajectory.html
# ##
# ## .. _RobotTrajectory:
# ##    http://docs.ros.org/noetic/api/moveit_msgs/html/msg/RobotTrajectory.html
# ##
# ## .. _rospy:
# ##    http://docs.ros.org/noetic/api/rospy/html/
# ## CALL_SUB_TUTORIAL imports
# ## CALL_SUB_TUTORIAL setup
# ## CALL_SUB_TUTORIAL basic_info
# ## CALL_SUB_TUTORIAL plan_to_joint_state
# ## CALL_SUB_TUTORIAL plan_to_pose
# ## CALL_SUB_TUTORIAL plan_cartesian_path
# ## CALL_SUB_TUTORIAL display_trajectory
# ## CALL_SUB_TUTORIAL execute_plan
# ## CALL_SUB_TUTORIAL add_box
# ## CALL_SUB_TUTORIAL wait_for_scene_update
# ## CALL_SUB_TUTORIAL attach_object
# ## CALL_SUB_TUTORIAL detach_object
# ## CALL_SUB_TUTORIAL remove_object
# ## END_TUTORIAL
