import rospy
from tf import TransformListener
import tf
import geometry_msgs.msg


rospy.init_node("ur10e_pose", anonymous=True)


class posePublisher:
    def __init__(self, *args):

        self.tf_listener_ = TransformListener()
        # self.pub = rospy.Publisher("ur10e/pose_publisher", String, queue_size=10)
        self.pub = rospy.Publisher("ur10e/pose", geometry_msgs.msg.PoseStamped, queue_size=10)

    def pub_end_effector_pose(self):
        #  t = self.tf_listener_.getLatestCommonTime("/base_link_inertia", "/wrist_3_link")
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():  # LOOP to give it time to recover the information asked
            try:
                t = rospy.Time(0)
                (position, quat) = self.tf_listener_.lookupTransform("/base_link", "/wrist_3_link", t)
                # self.pub.publish(position, quat)

                # construct message
                pose_stamped = geometry_msgs.msg.PoseStamped()
                pose_stamped.header.stamp = rospy.Time.now()
                pose_stamped.header.frame_id = "base_link/wrist_3_link"

                pose_stamped.pose.position.x = position[0]
                pose_stamped.pose.position.y = position[1]
                pose_stamped.pose.position.z = position[2]

                pose_stamped.pose.orientation.x = quat[0]
                pose_stamped.pose.orientation.y = quat[1]
                pose_stamped.pose.orientation.z = quat[2]
                pose_stamped.pose.orientation.w = quat[3]
                self.pub.publish(pose_stamped)
                # print("Positioń: ", position)
                # print("Quaternion ", quat)
                # break # if there is no exception you can break :)
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            rate.sleep()


if __name__ == '__main__':
    try:
        pose_pub = posePublisher()
        pose_pub.pub_end_effector_pose()
    except rospy.ROSInterruptException:
        pass
