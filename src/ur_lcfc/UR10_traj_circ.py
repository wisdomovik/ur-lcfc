# Sample trajectory
# Requires Wily Ponce Functions
# Arguments to change
# v =   mean speed in m/s
# x =   pose imput of original trajectory (linear)
# xo =  coordinate x of obstacle O
# R =   distance in the trajectory to avoid
# h =   height from 0 to R, normal to the trajectory at the point O
# R2 =  radius of the rising avoid trjectory
# R3 =  radius of the convergence avoid trajectory to the original

import numpy as np
import math as m
from math import pi
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from ur_lcfc_lib import Traj

# Speed
v = 0.25  # 250mm/s

# x =   [0.4 0.05 0.05 0 0 0]
xo = -0.1
R = 0.2
h = 0.5 * R
R2 = 0.5 * R
R3 = 1 * R
degree = 180 / pi

x0 = np.array([[0.4, -0.4, 0.15, 0, -180 * degree, 90 * degree]])
xf = np.array([[0.5, 0.4, 0.35, 0, -180 * degree, 90 * degree]])

# t=0:0.01:10
t = np.arange(0, 10, .01)

x0t = np.transpose(x0)
xi = np.concatenate((x0t, xf.transpose()), axis=1)

# #  Compute initial waypoints for arcAvoid
# a, b, c, d = Traj.arc_avoid(xi, -0.1, R, h, R2, R3)
# a2, b2, c2, d2 = Traj.arc_avoid(xi, 0.2, R, 0.5 * R, R2, R3)

# a, b, c, d = Traj.arc_avoid_min(xi, 0.2, R, 0.5 * R, R2, R3)
# print(a)
# print(a.shape)

# a2, b2, c2, _ = Traj.arc_avoid_min_epi(xi, 0.1, R, h, R2)
a2, b2, c2, _ = Traj.arc_avoid_min(xi, -0.1, R, h, R2, R3)

A = x0[0][0:3]
B = xf[0][0:3]
vecAB = B - A
print('VecAB =', vecAB)
b3 = Traj.rotate_points(b2, m.pi / 4, vecAB)
a2t = np.transpose(a2)
# print(a2t[:,0])
print(a2)
print(a2.shape)


# Testing x6pose 2 pose msg
x1 = [1, 2, 3, m.pi / 2, 0, 0]
x11 = np.array([x1]).transpose()
x12 = x11
matpose1 = np.concatenate((x11, x12), axis=1)
print('np.concatenate((x11, x12), axis=0)')
print(matpose1)
print(matpose1.shape)
# tt = Traj.times_at_wp(a2, v)
# print('time')
# print(tt)
# # print(d)

# fig = plt.figure(figsize=(4, 4))
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(c2[0], c2[1], c2[2])
# ax.scatter(a2[0], a2[1], a2[2])
# ax.scatter(b3[0], b3[1], b3[2])
# plt.show()

