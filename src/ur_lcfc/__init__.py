# USED TU INIT A 'PACKAGE'
'''
https://stackoverflow.com/questions/4383571/importing-files-from-different-folder
application
├── app
│   └── folder
│       └── file.py
└── app2
    └── some_folder
        └── some_file.py
I want to import some functions from file.py in some_file.py

        from application.app.folder.file import func_name


 Just make sure folder also contains an __init__.py, this allows it to be 
 included as a package. Not sure why the other answers talk about PYTHONPATH.

'''