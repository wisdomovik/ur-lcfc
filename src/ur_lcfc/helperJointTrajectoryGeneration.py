#!/usr/bin/env python
"""
Based on exampleHelperJointTrajectoryGeneration of Matlab Robotics TB

Author: Wily Ponce
Date: 2021.11.16

"""

import numpy as np
from scipy import interpolate
import copy


def pchipJointTrajectoryGeneration(tWaypoints, qWaypoints, tt, qdout=True, correction=True):
    """
    EXAMPLEHELPERJOINTTRAJECTORYGENERATION Generate joint trajectory from
       given time and joint configuration waypoints.

       Input:
       tWaypoints: 1-by-n vector, where n is number of waypoints
       qWaypoints: n-by-ndof matrix, where ndof is the number of degrees of
                   freedom.
        tt: 1-by-np vector containing time points where the piece-wise
           polynomial trajectories are evaluated.

       Output:
       qDesired: joint positions, a (n+2)-by-ndof vector
       qdotDesired: joint velocities, a (n+2)-by-ndof vector
       qddotDesired: joint accelerations, a (n+2)-by-ndof vector
       tt: time points for joint trajectory, a 1-by-(n+2) vector

       Explanation for (n+2):
       To force the trajectory to start and end with zero velocities, the method
       will automatically prepend the first waypoint and append the last
       waypoint to the original user-supplied tWaypoints and qWaypoints.

       The trajectory is generated using |pchip| (Piecewise Cubic Hermite Interpolating
       Polynomial). |pchip| is not as smooth as |spline|, however, it guarantees
       that the interpolated joint position does not violate joint limits, as
       long as the waypoints do not.
    """
    # If there is only needed the initial q (NOT the qdot or qddot)
    if qdout is False:
        q = interpolate.pchip_interpolate(tWaypoints, qWaypoints, tt, axis=0)
        return q

    # Check if times are different (not repeated in the begining 2 points)
    FIRST_TWO_EQUAL = False
    if tt[0] == tt[1]:
        FIRST_TWO_EQUAL = True
        # Delete first values
        tWaypoints = np.delete(tWaypoints, [0], 0)  #
        qWaypoints = np.delete(qWaypoints, [0], 0)  # 
    ndof = qWaypoints.shape[1]
    dt = 0.01
    Q = np.vstack((qWaypoints[0, :], qWaypoints, qWaypoints[-1, :]))
    T = np.hstack((0, tWaypoints + dt, tWaypoints[-1] + 2 * dt))
    tt = np.hstack((0, tt + dt, tt[-1] + 2 * dt))

    pp_pos = interpolate.PchipInterpolator(T, Q, axis=0)
    pp_pos.c  # pchip coefficients
    # pp_pos.c.shape = [ ncoef, npiece, dof ]
    pieces = pp_pos.c.shape[1]  # length of T - 1, like segments
    ncoefs = pp_pos.c.shape[0]  # number of coefficients per q i.e. for cubic = 4 (c1 c2 c3 c4)

    # #  STRUCTURING AS IN MATLAB
    # #  Matlab like matrixes for coeficients
    # coefs_vel_matlab = np.zeros((ndof * pieces, 3))
    # coefs_acc_matlab = np.zeros((ndof * pieces, 2))
    # coefs_pos_matlab = np.zeros((ndof * pieces, 4))
    # # Composing coefficients matrix as in matlab
    # for j in range(pieces):
    #     for k in range(ndof):
    #         c = pp_pos.c[:, j, k]
    #         coefs_pos_matlab[j * ndof + k, :] = c
    # # Decomposing as in matlab to obtain vel and acc
    # for k in range(ndof):
    #     for j in range(pieces):
    #         c = coefs_pos_matlab[k * pieces + j, :]
    #         coefs_vel_matlab[k * pieces + j, :] = np.array([3 * c[0], 2 * c[1], c[2]])
    #         coefs_acc_matlab[k * pieces + j, :] = np.array([6 * c[0], 2 * c[1]])


    # Sccipy interpolation pchip coefs matrixis like
    # pp_pos.c.shape = [ ncoefs, npieces, ndof ]
    coefs_vel = np.zeros((ncoefs - 1, pieces, ndof))  # - 1 because is of order 3
    coefs_acc = np.zeros((ncoefs - 2, pieces, ndof))  # -2  to become of order 2

    # Composing coefficients matrix as in matlab
    for j in range(pieces):
        for k in range(ndof):
            c = pp_pos.c[:, j, k]  # Otain the coefficients for given piece, ndof
            # Append to the coeffc with chape as in pchip.c
            coefs_vel[:, j, k] = np.array([3 * c[0], 2 * c[1], c[2]])
            coefs_acc[:, j, k] = np.array([6 * c[0], 2 * c[1]])

    # Deep copy pchip object otherwise it will copy itself to the new var name
    pp_vel = copy.deepcopy(pp_pos)
    # pp_vel.order = 3
    pp_vel.c = coefs_vel

    pp_acc = copy.deepcopy(pp_pos)
    # pp_acc.order = 2
    pp_acc.c = coefs_acc

    # Evaluate polinomyal pchip at tt
    q = pp_pos(tt)
    qdot = pp_vel(tt)
    qddot = pp_acc(tt)

    # Wilys correction delete first and last row ; from time las 2 rows
    if correction:
        q = np.delete(q, [0, -1], 0)
        qdot = np.delete(qdot, [0, -1], 0)
        qddot = np.delete(qddot, [0, -1], 0)

        tt = np.delete(tt, [1, -1], 0)

    return q, qdot, qddot, tt

    #SAMPLE CODE USED IN MOVE GROUP PYTHON
    #     # # # --------------------
    # input("continue press enter: 11")
    # # # # ===== PCHIP JOINT TRAJECTORY GENERATION NO CORRECTION    (x,    y,   xx)
    # qs, qds, qdds, ttchip = pchipJointTrajectoryGeneration(ttm, qq, ttm, correction=False)
    # # print("Velocity \n", qds)

    # print(ttchip)
    # input("continue press enter: 22")

    # # # #
    # # # # ===== GENERATE JOINT TRAJECTORY POINT ARRAY TO INSERT INTO THE CARTESIAN PLAN
    # pchip_points = MiscUR.posvelacc_2_joint_trajectory_point_array(qs, qds, qdds, ttchip)
    # print("len pchip points array", len(pchip_points))
    # print(pchip_points)
    # input("continue press enter: 22.5")
    # # Copy original moveit cartesian_plan
    # cartesian_plan_pchip = copy.deepcopy(cartesian_plan)
    # # COPY THE NEW CARTESIAN POINTS
    # cartesian_plan_pchip.joint_trajectory.points = pchip_points
    # planned_points_info(pchip_points, plot=True, title="PCHIP cartesian points NO crrection")

def helper_test():
    # Creating some waypoints with 6 dof
    wp1 = np.linspace(0, 2.5, 6)
    wp2 = wp1 * -1.0 + 0.25
    wp3 = wp2 * 0.5
    wp4 = wp2 * 0.1
    wp5 = wp3 * -1
    wp6 = np.linspace(0.1, 3, 6)

    qWaypoints = np.vstack((wp1, wp2, wp3, wp4, wp5, wp6, wp1))
    """
            [[ 0.     0.5    1.     1.5    2.     2.5  ]
             [ 0.25  -0.25  -0.75  -1.25  -1.75  -2.25 ]
             [ 0.125 -0.125 -0.375 -0.625 -0.875 -1.125]
             [ 0.025 -0.025 -0.075 -0.125 -0.175 -0.225]
             [-0.125  0.125  0.375  0.625  0.875  1.125]
             [ 0.1    0.68   1.26   1.84   2.42   3.   ]
             [ 0.     0.5    1.     1.5    2.     2.5  ]]
    """

    # Time waypoints
    tWaypoints = np.array([0, 1, 2, 3, 4, 5, 6])
    dt = 0.1

    # Times at the output that evaluates the given pchip functions
    tt = np.arange(0, tWaypoints[-1] + dt, dt)
    print(type(tt))
    print(tt.shape)

    print(qWaypoints)

    # ===========================================
    q, qdot, qddot, tt = pchipJointTrajectoryGeneration(tWaypoints, qWaypoints, tt, qdout=True)
    # print("qdesired \n", qdesired)
    # print("qdesired[:, 0] \n", qdesired[:, 0])
    print("qdesired.shape \n", q.shape)
    print("qdesired \n", q)
    print("qdot_desired.shape \n", qdot.shape)
    print("qdot_desired\n", qdot)

    if qdot[19, 3] == qddot[19, 3]:
        print("WTFF!!!!")
    xdd = qdot[19:25, :]
    print("qddot_desired[19:25, :] \n", xdd)
    print("qddot_desired.shape \n", qddot.shape)
    print("qddot_desired \n", qddot)

def main():

    helper_test()

if __name__ == "__main__":
    main()

