"""
If rosbag python execute problems not finding rosbag, install following module:
sudo apt update
sudo apt install python3-rosbag


Script to extract messages on desired topics, and save them in a CSV file. Easy to modify as per requirements.
This script collects data from all the bag files in the directory passed as the first
argument when launching. In particular, it collects the rostopics listed in the
concerned list and generates a CSV file containing the same. The readings are triggered
by the trigger topic.

Author: Dhruv Ilesh Shah
shahdhruv@cmu.edu | dhruv.shah@iitb.ac.in
"""

import rosbag
import numpy as np
import sys
import os
import csv
import yaml
import tkinter as tk
from tkinter import filedialog, messagebox
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.cm as cmx
from mpl_toolkits.mplot3d import Axes3D
import urplot

root = tk.Tk()
root.withdraw()

# Directory to cherche files 
INIT_dire = '~/bagfiles'
INIT_dire = '~/tests211123'
INIT_dire = '/home/wily/Dropbox/Ubuntu-LCFC/Trajectories/tests211119'
INIT_dire = '/home/wily/Dropbox/Ubuntu-LCFC/Trajectories/sims'
INIT_dire = '/home/wily/Dropbox/Ubuntu-LCFC/Trajectories/tests211123'

rostopics = ["/ur10e/move_group_message",
             "/ur10e/pose",
             "/ur10e/joint_states",
             "/joint_states",
             "/move_group/display_planned_path",
             "/scaled_pos_joint_traj_controller/follow_joint_trajectory/goal",
             "/scaled_pos_joint_traj_controller/follow_joint_trajectory/feedback"]

rostopics_dict = {"/ur10e/move_group_message": "message",
                  "/ur10e/pose": "Secs, x, y, z, qx, qy, qz, qw",
                  "/ur10e/joint_states": "js_secs, q1, q2, q3, q4, q5, q6, v1, v2, v3, v4, v5, v6, e1, e2, e3, e4, e5, e6",
                  "/joint_states": "js_secs, q1, q2, q3, q4, q5, q6, v1, v2, v3, v4, v5, v6, e1, e2, e3, e4, e5, e6",
                  "/move_group/display_planned_path": "js_secs, secs, q1, q2, q3, q4, q5, q6, v1, v2, v3, v4, v5, v6, a1, a2, a3, a4, a5, a6, e1, e2, e3, e4, e5, e6",
                  "/scaled_pos_joint_traj_controller/follow_joint_trajectory/goal": "js_secs, secs, q1, q2, q3, q4, q5, q6, v1, v2, v3, v4, v5, v6, a1, a2, a3, a4, a5, a6, e1, e2, e3, e4, e5, e6",
                  "/scaled_pos_joint_traj_controller/follow_joint_trajectory/feedback": "fb_secs, \
dq1, dq2, dq3, dq4, dq5, dq6, dv1, dv2, dv3, dv4, dv5, dv6, da1, da2, da3, da4, da5, da6, \
cq1, cq2, cq3, cq4, cq5, cq6, cv1, cv2, cv3, cv4, cv5, cv6, ca1, ca2, ca3, ca4, ca5, ca6, \
erq1, erq2, erq3, erq4, erq5, erq6, erv1, erv2, erv3, erv4, erv5, erv6, era1, era2, era3, era4, era5, era6 "}    # d: desired, c: current, er: error     
# "rostopic" versioned for CSV (not real rostopic in ros)
rostopics_info = ["Message frome move_group interface",
                  "secs, x, y, z, quat[x,y,z,w]",
                  "js_secs, qpos x 6, qvel x 6, effort x 6",
                  "js_secs, secs, qpos x 6, + qvel x 6, qacc x 6, effort x 6"
                  "js_secs, secs, qpos x 6, + qvel x 6, qacc x 6, effort x 6"]
# Global variables
freq = 20;
planned_goal_pos = np.zeros((1, 6))
planned_goal_vel = np.zeros((1, 6))
planned_goal_acc = np.zeros((1, 6))
planned_goal_eff = np.zeros((1, 6))
planned_goal_time = np.zeros((1, 1))

planned_pos = np.zeros((1, 6))
planned_vel = np.zeros((1, 6))
planned_acc = np.zeros((1, 6))
planned_eff = np.zeros((1, 6))
planned_time = np.zeros((1, 1))

real_pos = np.zeros((1, 6))
real_vel = np.zeros((1, 6))
real_eff = np.zeros((1, 6))
real_time = np.zeros((1, 1))

pose_pos = np.zeros((1, 3))
pose_quat = np.zeros((1, 4))
pose_time = np.zeros((1, 1))

print(chr(27) + "[2J")
local_dir_path = os.path.dirname(os.path.realpath(__file__))
print("Entering", local_dir_path)

def delete_firstrow(nparray):
    nparray = np.delete(nparray, 0, 0)
    return nparray


def parse_planned_goal_points(points_arr):
    # Similar to parse_planned_points, but it differs in that we have to change order
    # of the first three data points due to order differens in joints names
    # Parse points messages from array 2 csv and to local variables planed_xxx
    # UR10e 6 axes
    # trajectory_msgs/JointTrajectoryPoint Message
    """
    float64[] positions
    float64[] velocities
    float64[] accelerations
    float64[] effort
    duration time_from_start
    # joint_names: 
                #   - elbow_joint               ->  - shoulder_pan_joint
                #   - shoulder_lift_joint       ->  Same
                #   - shoulder_pan_joint        ->  - elbow_joint 
                #   - wrist_1_joint             ->  Same
                #   - wrist_2_joint             ->  Same
                #   - wrist_3_joint             ->  Same
    """
    global planned_goal_pos, planned_goal_vel, planned_goal_acc, planned_goal_time

    # topic path "/move_group/display_planned_path/trajectory[0]/joint_trajectory/points"
    _topic_name = "/scaled_pos_joint_traj_controller/follow_joint_trajectory/goal/JointTrajectoryPoint"

    #CSV for this topic separate file
    csv_temp = filename + _topic_name.replace('/', '_') + '.csv'

    data2 = [_topic_name] + rostopics_dict[topic].split(',')
    csv_append(csv_temp, data2)

    for _point in points_arr:
        # Seconds and nanoseconds
        ss = _point.time_from_start.secs
        ns = _point.time_from_start.nsecs
        # Seconds.nanoseconds
        t_secs = ss + ns * 10**(-9)

        veli = [0, 0, 0, 0, 0, 0]
        acci = [0, 0, 0, 0, 0, 0]

        # Parse time + POSITIONS to csv
        # Changing the order of Pan and Shoulder joints, thats why is [2] [1] [0]
        posi = [_point.positions[2], _point.positions[1], _point.positions[0],
                _point.positions[3], _point.positions[4], _point.positions[5]]

        # Parse time + VELOCITIES to csv
        if len(_point.velocities) == 6:
            veli = [_point.velocities[2], _point.velocities[1], _point.velocities[0],
                    _point.velocities[3], _point.velocities[4], _point.velocities[5]]

        # Parse time + ACCELERATIONS to csv
        if len(_point.accelerations) == 6:
            acci = [_point.accelerations[2], _point.accelerations[1], _point.accelerations[0],
                    _point.accelerations[3], _point.accelerations[4], _point.accelerations[5]]

        # Parse time + EFFORT to csv
        if len(_point.effort) == 6:  # Check if it has the right length
            effi = [_point.effort[2], _point.effort[1], _point.effort[0],
                    _point.effort[3], _point.effort[4], _point.effort[5]]
        else:
            effi = [0, 0, 0, 0, 0, 0]

        # Whole line with time + positions + velocities + accc + effort
        #           topic  , jointstatestime, tt, q     ,  qdot, qddot
        data = [_topic_name, _t_secs_js, t_secs] + posi + veli + acci + effi
        if CSV_ON:
            csv_append(csv_file, data)
            csv_append(csv_temp, data)

        # Store in GLOBAL variables the whole matrixes of Positions, Vel, Acc, Time
        planned_goal_pos = np.concatenate((planned_goal_pos, np.array([posi])), axis=0)
        planned_goal_vel = np.concatenate((planned_goal_vel, np.array([veli])), axis=0)
        planned_goal_acc = np.concatenate((planned_goal_acc, np.array([acci])), axis=0)
        planned_goal_time = np.concatenate((planned_goal_time, np.array([[t_secs]])), axis=0)

    planned_goal_pos = delete_firstrow(planned_goal_pos)
    planned_goal_vel = delete_firstrow(planned_goal_vel)
    planned_goal_acc = delete_firstrow(planned_goal_acc)
    planned_goal_time = delete_firstrow(planned_goal_time)

def parse_planned_points(points_arr):
    # Parse points messages from array 2 csv and to local variables planed_xxx
    # UR10e 6 axes
    # trajectory_msgs/JointTrajectoryPoint Message
    """
    float64[] positions
    float64[] velocities
    float64[] accelerations
    float64[] effort
    duration time_from_start
    """
    global planned_pos, planned_vel, planned_acc, planned_time

    # topic path "/move_group/display_planned_path/trajectory[0]/joint_trajectory/points"
    _topic_name = "/move_group/display_planned_path/JointTrajectoryPoint"

    #CSV for this topic separate file
    csv_temp = filename + _topic_name.replace('/', '_') + '.csv'

    data2 = [_topic_name] + rostopics_dict[topic].split(',')
    csv_append(csv_temp, data2)

    for _point in points_arr:
        # Seconds and nanoseconds
        ss = _point.time_from_start.secs
        ns = _point.time_from_start.nsecs
        # Seconds.nanoseconds
        t_secs = ss + ns * 10**(-9)

        # Parse time + POSITIONS to csv
        posi = [_point.positions[0], _point.positions[1], _point.positions[2],
                _point.positions[3], _point.positions[4], _point.positions[5]]

        # Parse time + VELOCITIES to csv
        veli = [_point.velocities[0], _point.velocities[1], _point.velocities[2],
                _point.velocities[3], _point.velocities[4], _point.velocities[5]]

        # Parse time + ACCELERATIONS to csv
        acci = [_point.accelerations[0], _point.accelerations[1], _point.accelerations[2],
                _point.accelerations[3], _point.accelerations[4], _point.accelerations[5]]

        # Parse time + EFFORT to csv
        if len(_point.effort) == 6:  # Check if it has the right length
            effi = [_point.effort[0], _point.effort[1], _point.effort[2],
                    _point.effort[3], _point.effort[4], _point.effort[5]]
        else:
            effi = [0, 0, 0, 0, 0, 0]

        # Whole line with time + positions + velocities + accc + effort
        #           topic  , jointstatestime, tt, q     ,  qdot, qddot
        data = [_topic_name, _t_secs_js, t_secs] + posi + veli + acci + effi
        if CSV_ON:
            csv_append(csv_file, data)
            csv_append(csv_temp, data)


        # Store in GLOBAL variables the whole matrixes of Positions, Vel, Acc, Time
        planned_pos = np.concatenate((planned_pos, np.array([posi])), axis=0)
        planned_vel = np.concatenate((planned_vel, np.array([veli])), axis=0)
        planned_acc = np.concatenate((planned_acc, np.array([acci])), axis=0)
        planned_time = np.concatenate((planned_time, np.array([[t_secs]])), axis=0)

    planned_pos = delete_firstrow(planned_pos)
    planned_vel = delete_firstrow(planned_vel)
    planned_acc = delete_firstrow(planned_acc)
    planned_time = delete_firstrow(planned_time)


def csv_append(filename, data, *header):
    # APPEND DATA FROM LISTS i.e. data = ['Afghanistan', 652090, 'AF', 'AFG']
    isExist = os.path.exists(filename)
    # Overriding is Exist
    isExist = True
    if len(header) == 0:
        # Using *header rtuns tuple
        header = data

    if isExist:
        # print('File exists: ', filename)

        # APPEND NEW LINES
        # 'a' to append, 'w' to rewrite
        with open(filename, 'a', newline='', encoding='UTF8') as f:
            # writer = csv.writer(f, dialect='excel-tab')
            writer = csv.writer(f, delimiter=',')

            # write/append new data
            res = writer.writerow(data)
    else:
        print('FIle does not exist, creating:', filename)

        # WRITIGN FIRST LINE OR HEADER LINE
        with open(filename, 'w', newline='', encoding='UTF8') as f:
            writer = csv.writer(f, dialect='excel-tab')
            #writer = csv.writer(f, delimiter=',')

            # write the header
            if len(header) == 1:
                res = writer.writerow(header[0])  # Using *headr rtuns tuple
            else:
                res = writer.writerow(header)  # Using *headr
            # write/append new data
            res = writer.writerow(data)
    return res


# GET FILE FROM DIALOG
LST_Types = [("bag rosbag", ".bag")]
filename = filedialog.askopenfilename(filetypes=LST_Types, initialdir=INIT_dire)
CSV_ON = messagebox.askyesno(title='Parse to CSV',
                             message='Would you like to parse to CSV? \n (just plot)')

if ".bag" not in filename:  # Check if the file is a bag or not
    print("File is not a .bag: ", filename)
    sys.exit()  # File it is not a bag file, then skip this, go next
if filename[-1] != "g":
    print("File is not a .bag g: ", filename)
    sys.exit()  # File it is not a bag file, then skip this, go next

print("Opening", filename)
bag = rosbag.Bag(os.path.join(local_dir_path, filename))
bag_info = yaml.load(bag._get_yaml_info())
print("Found bag with duration", bag_info['duration'], "seconds")
print("Parsing", filename)

if CSV_ON:
    csv_file = filename + ".csv"
    csvfile = open(csv_file, 'wb')
    writer = csv.writer(csvfile, delimiter=',')
    flag = 0
    row = []

_header = ["/rostpics"] + rostopics

# Flags
flag_js = True
flag_js2 = True
flag_pose = True
flag_plan = True
flag_goal = True
flag_feedback = True

# Init some ts
_t_secs_js = 0
_t_secs_pose = 0
_t_secs_feedback = 0

# Init times
tinit_pose_secs = 0
tinit_js_secs = 0
tinit_feedback = 0

# Start parsing
for topic, msg, t in bag.read_messages(topics=rostopics):
    # Read messages from the bag, optionally filtered by topic, timestamp and connection details.
    if topic in rostopics:

        if topic == "/ur10e/move_group_message":
            # print "Rad: ", msg[1].multi_array.layout.dim[0].size
            data = [topic, msg.data]
            if CSV_ON:
                csv_append(csv_file, data)
            if msg.data == "move_group_python_interface_ur10e Finished":
                flag = 0

        # ~============================================= = =  =  =   =
        # ~============================================= = =  =  =   =
        elif topic == "/ur10e/pose":
            # geometry_msgs.msg.PoseStamped
            # First time detecting topic joint_states, routine

            #CSV for this topic separate file
            csv_temp = filename + topic.replace('/', '_') + '.csv'

            if flag_pose:
                # Store once the joint names
                pose_info = "secs, xyz, quat.xyzw "  # seq
                data = [topic + "/info", pose_info]
                if CSV_ON:
                    csv_append(csv_file, data)
                    # copy header in first line of tem csv
                    data2 = [topic] + rostopics_dict[topic].split(',')
                    csv_append(csv_temp, data2)

                # Normalize time seconds
                tinit_pose_secs = msg.header.stamp.secs + msg.header.stamp.nsecs * 10**(-9)

                flag_pose = False  # Deactivate after first passing once here

            # Normalize time in seconds
            # Seconds and nanoseconds
            _ss = msg.header.stamp.secs
            _ns = msg.header.stamp.nsecs
            # Secons.nanoseconds - initial seconds
            _t_secs_pose = _ss + _ns * 10**(-9) - tinit_pose_secs

            # Extract local variables for pos xyz, quat xyzw,
            _xyz = [msg.pose.position.x,
                    msg.pose.position.y,
                    msg.pose.position.z]
            _quat = [msg.pose.orientation.x,
                     msg.pose.orientation.y,
                     msg.pose.orientation.z,
                     msg.pose.orientation.w]
            data = [_t_secs_pose] + _xyz + _quat
            data.insert(0, "/ur10e/pose")

            
            if CSV_ON:
                csv_append(csv_file, data)
                #In separete file
                csv_append(csv_temp, data)

            # Concatenate global variables
            # Store in GLOBAL variables the whole matrixes of Positions, Vel, Acc, Time
            pose_pos = np.append(pose_pos, np.array([_xyz]), axis=0)
            pose_quat = np.append(pose_quat, np.array([_quat]), axis=0)
            pose_time = np.append(pose_time, np.array([[_t_secs_pose]]), axis=0)

        # ~============================================= = =  =  =   =
        # ~============================================= = =  =  =   =
        elif topic == "/ur10e/joint_states":
            # Message:  sensor_msgs/JointState.msg

            #CSV for this topic separate file
            csv_temp = filename + topic.replace('/', '_') + '.csv'

            # First time detecting topic joint_states, routine
            if flag_js and len(msg.position) == 6:
                # Store once the joint names
                str1 = ""
                for name in msg.name:  # Make a cell with new lines (narrow cell)
                    str1 = str1 + name + "\n"
                data = [topic + "/names", str1]
                if CSV_ON:
                    csv_append(csv_file, data)
                    # copy header in first line of tem csv
                    data2 = [topic] + rostopics_dict[topic].split(',')
                    csv_append(csv_temp, data2)

                # Normalize time seconds
                tinit_js_secs = msg.header.stamp.secs + msg.header.stamp.nsecs * 10**(-9)

                flag_js = False  # Deactivate after first passing once here

            # positions
            if len(msg.position) == 6:
                # Compute new time
                # Seconds and nanoseconds
                _ss = msg.header.stamp.secs
                _ns = msg.header.stamp.nsecs
                # Secons.nanoseconds - initial seconds
                _t_secs_js = _ss + _ns * 10**(-9) - tinit_js_secs

                # print("debug msg joint states:", msg)
                # Extract local variables for position, vel and eff
                _js_position = [msg.position[0], msg.position[1], msg.position[2],
                                msg.position[3], msg.position[4], msg.position[5]]
                if len(msg.velocity) == 6:
                    _js_velocity = [msg.velocity[0], msg.velocity[1], msg.velocity[2],
                                    msg.velocity[3], msg.velocity[4], msg.velocity[5]]
                else:
                    _js_velocity = [0, 0, 0, 0, 0, 0]
                if len(msg.effort) == 6:
                    _js_effort = [msg.effort[0], msg.effort[1], msg.effort[2],
                                  msg.effort[3], msg.effort[4], msg.effort[5]]
                else:
                    _js_effort = [0, 0, 0, 0, 0, 0]
                data = [_t_secs_js] + _js_position + _js_velocity + _js_effort
                data.insert(0, "/ur10e/joint_states")
                if CSV_ON:
                    csv_append(csv_file, data)
                    # copy data in separate file
                    csv_append(csv_temp, data)

                # Concatenate global variables
                # Store in GLOBAL variables the whole matrixes of Positions, Vel, Acc, Time
                real_pos = np.append(real_pos, np.array([_js_position]), axis=0)
                real_vel = np.append(real_vel, np.array([_js_velocity]), axis=0)
                real_eff = np.append(real_eff, np.array([_js_effort]), axis=0)
                real_time = np.append(real_time, np.array([[_t_secs_js]]), axis=0)

        # ~============================================= = =  =  =   =
        # ~============================================= = =  =  =   =
        elif topic == "/joint_states":
            # Message:  sensor_msgs/JointState.msg

            ts = 1 / freq # Time step

            #CSV for this topic separate file
            csv_temp = filename + topic.replace('/', '_') + '.csv'

            # First time detecting topic joint_states, routine
            if flag_js2 and len(msg.position) == 6:
                # Store once the joint names
                str1 = ""
                list1 = []
                for name in msg.name:  # Make a cell with new lines (narrow cell)
                    str1 = str1 + name + "\n"
                    list1.append(name)
                list2 = list1
                list2[0] = list1[2]
                list2[2] = list1[0]
                data = [topic + "/names", list2]
                if CSV_ON:
                    csv_append(csv_file, data)
                    # copy header in first line of tem csv
                    data2 = [topic] + rostopics_dict[topic].split(',')
                    csv_append(csv_temp, data2)

                # Normalize time seconds
                tinit_js_secs2 = msg.header.stamp.secs + msg.header.stamp.nsecs * 10**(-9)

                tjsprev = 0

                flag_js2 = False  # Deactivate after first passing once here

            # positions
            if len(msg.position) == 6:
                # Compute new time
                # Seconds and nanoseconds
                _ss = msg.header.stamp.secs
                _ns = msg.header.stamp.nsecs
                # Secons.nanoseconds - initial seconds
                _t_secs_js2 = _ss + _ns * 10**(-9) - tinit_js_secs2
                tjs = _t_secs_js2

                # Check if the timestep has passed to skip data or copy the correspondant
                if tjs > tjsprev + ts:
                    tjsprev = tjs

                    # print("debug msg joint states:", msg)
                    # Extract local variables for position, vel and eff
                    _js_position = [msg.position[2], msg.position[1], msg.position[0],
                                    msg.position[3], msg.position[4], msg.position[5]]
                    if len(msg.velocity) == 6:
                        _js_velocity = [msg.velocity[2], msg.velocity[1], msg.velocity[0],
                                        msg.velocity[3], msg.velocity[4], msg.velocity[5]]
                    else:
                        _js_velocity = [0, 0, 0, 0, 0, 0]
                    if len(msg.effort) == 6:
                        _js_effort = [msg.effort[2], msg.effort[1], msg.effort[0],
                                      msg.effort[3], msg.effort[4], msg.effort[5]]
                    else:
                        _js_effort = [0, 0, 0, 0, 0, 0]
                    data = [_t_secs_js2] + _js_position + _js_velocity + _js_effort
                    data.insert(0, "/joint_states")
                    if CSV_ON:
                        csv_append(csv_file, data)
                        # copy data in separate file
                        csv_append(csv_temp, data)

                    # Concatenate global variables
                    # Store in GLOBAL variables the whole matrixes of Positions, Vel, Acc, Time
                    real_pos = np.append(real_pos, np.array([_js_position]), axis=0)
                    real_vel = np.append(real_vel, np.array([_js_velocity]), axis=0)
                    real_eff = np.append(real_eff, np.array([_js_effort]), axis=0)
                    real_time = np.append(real_time, np.array([[_t_secs_js2]]), axis=0)

        # ~============================================= = =  =  =   =
        # ~============================================= = =  =  =   =
        elif topic == "/move_group/display_planned_path":

            #CSV for this topic separate file
            csv_temp = filename + topic.replace('/', '_') + '.csv'

            if flag_plan:
                # Append topic joint names if encounterd for the first time
                _joint_names = msg.trajectory[0].joint_trajectory.joint_names
                str1 = ""
                for name in _joint_names:  # Make a cell with new lines (narrow cell)
                    str1 = str1 + name + "\n"
                data = [topic + "/names", str1]
                if CSV_ON:
                    csv_append(csv_file, data)
                    # copy header in first line of tem csv
                    data2 = [topic] + rostopics_dict[topic].split(',')
                    csv_append(csv_temp, data2)

                # Deactivate ONCE flag pass
                flag_plan = False

            # Append last time from joint states topic parse
            data = [topic + "/joint_states_topic_time"] + [_t_secs_js]
            if CSV_ON:
                csv_append(csv_file, data)
                # copy header in first line of tem csv
                csv_append(csv_temp, data)

            # Append the JointTrajectoryPoint s and parse them 2 CSV and Global var
            parse_planned_points(msg.trajectory[0].joint_trajectory.points)

        # ~============================================= = =  =  =   =
        # ~============================================= = =  =  =   =
        elif topic == "/scaled_pos_joint_traj_controller/follow_joint_trajectory/goal":
            # Type: control_msgs/FollowJointTrajectoryActionGoal

            #CSV for this topic separate file
            csv_temp = filename + topic.replace('/', '_') + '.csv'

            if flag_goal:
                # Append topic joint names if encounterd for the first time and change orignal order
                # joint_names: 
                #   - elbow_joint               ->  - shoulder_pan_joint
                #   - shoulder_lift_joint       ->  Same
                #   - shoulder_pan_joint        ->  - elbow_joint 
                #   - wrist_1_joint             ->  Same
                #   - wrist_2_joint             ->  Same
                #   - wrist_3_joint             ->  Same

                _joint_names = msg.goal.trajectory.joint_names
                elbow = _joint_names[0]
                shoulder_pan = _joint_names[2]
                # Reorder joints order to match 
                _joint_names[0] = shoulder_pan
                _joint_names[2] = elbow

                str1 = ""
                for name in _joint_names:  # Make a cell with new lines (narrow cell)
                    str1 = str1 + name + "\n"
                data = [topic + "/names", str1]
                if CSV_ON:
                    csv_append(csv_file, data)
                    data2 = [topic] + rostopics_dict[topic].split(',')
                    csv_append(csv_temp, data2)

                # Deactivate ONCE flag pass
                flag_goal = False

            # Append last time from joint states topic parse
            data = [topic + "/joint_states_topic_time", _t_secs_js]
            # data = [topic + "/header_time"]
            if CSV_ON:
                csv_append(csv_file, data)
                # copy header in first line of tem csv
                csv_append(csv_temp, data)

            # Append the JointTrajectoryPoint s and parse them 2 CSV and Global var
            parse_planned_goal_points(msg.goal.trajectory.points)

        # ~============================================= = =  =  =   =
        # ~============================================= = =  =  =   =
        elif topic == "/scaled_pos_joint_traj_controller/follow_joint_trajectory/feedback":
            # control_msgs/FollowJointTrajectoryActionFeedback

            #CSV for this topic separate file
            csv_temp = filename + topic.replace('/', '_') + '.csv'

            # First time detecting topic joint_states, routine
            if flag_feedback:
                # Store once the joint names
                str1 = ""
                for name in msg.feedback.joint_names:  # Make a cell with new lines (narrow cell)
                    str1 = str1 + name + "\n"
                data = [topic + "/names", str1]
                if CSV_ON:
                    csv_append(csv_file, data)
                    data2 = [topic] + rostopics_dict[topic].split(',')
                    csv_append(csv_temp, data2)

                # Init time to later Normalize time seconds
                tinit_feedback = msg.feedback.header.stamp.secs + msg.feedback.header.stamp.nsecs * 10**(-9)
                # Correct it with LAST _t_secs_js which normally is already up
                tinit_feedback = tinit_feedback - _t_secs_js

                flag_feedback = False  # Deactivate after first passing once here

            # Init desired arrays
            _desired_vel = [0, 0, 0, 0, 0, 0]
            _desired_acc = [0, 0, 0, 0, 0, 0]

            _desired_pos = [msg.feedback.desired.positions[0], msg.feedback.desired.positions[1],
                            msg.feedback.desired.positions[2], msg.feedback.desired.positions[3],
                            msg.feedback.desired.positions[4], msg.feedback.desired.positions[5]]
            if len(msg.feedback.desired.velocities) == 6:
                _desired_vel = [msg.feedback.desired.velocities[0], msg.feedback.desired.velocities[1],
                                msg.feedback.desired.velocities[2], msg.feedback.desired.velocities[3],
                                msg.feedback.desired.velocities[4], msg.feedback.desired.velocities[5]]
            if len(msg.feedback.desired.accelerations) == 6:
                _desired_acc = [msg.feedback.desired.accelerations[0], msg.feedback.desired.accelerations[1],
                                msg.feedback.desired.accelerations[2], msg.feedback.desired.accelerations[3],
                                msg.feedback.desired.accelerations[4], msg.feedback.desired.accelerations[5]]

            # Init actual arrays
            _actual_vel = [0, 0, 0, 0, 0, 0]
            _actual_acc = [0, 0, 0, 0, 0, 0]

            _actual_pos = [msg.feedback.actual.positions[0], msg.feedback.actual.positions[1],
                           msg.feedback.actual.positions[2], msg.feedback.actual.positions[3],
                           msg.feedback.actual.positions[4], msg.feedback.actual.positions[5]]
            if len(msg.feedback.actual.velocities) == 6:
                _actual_vel = [msg.feedback.actual.velocities[0], msg.feedback.actual.velocities[1],
                               msg.feedback.actual.velocities[2], msg.feedback.actual.velocities[3],
                               msg.feedback.actual.velocities[4], msg.feedback.actual.velocities[5]]
            if len(msg.feedback.actual.accelerations) == 6:
                _actual_acc = [msg.feedback.actual.accelerations[0], msg.feedback.actual.accelerations[1],
                               msg.feedback.actual.accelerations[2], msg.feedback.actual.accelerations[3],
                               msg.feedback.actual.accelerations[4], msg.feedback.actual.accelerations[5]]

            # Init error arrays
            _error_vel = [0, 0, 0, 0, 0, 0]
            _error_acc = [0, 0, 0, 0, 0, 0]

            _error_pos = [msg.feedback.error.positions[0], msg.feedback.error.positions[1],
                          msg.feedback.error.positions[2], msg.feedback.error.positions[3],
                          msg.feedback.error.positions[4], msg.feedback.error.positions[5]]
            if len(msg.feedback.error.velocities) == 6:
                _error_vel = [msg.feedback.error.velocities[0], msg.feedback.error.velocities[1],
                              msg.feedback.error.velocities[2], msg.feedback.error.velocities[3],
                              msg.feedback.error.velocities[4], msg.feedback.error.velocities[5]]
            if len(msg.feedback.error.accelerations) == 6:
                _error_acc = [msg.feedback.error.accelerations[0], msg.feedback.error.accelerations[1],
                              msg.feedback.error.accelerations[2], msg.feedback.error.accelerations[3],
                              msg.feedback.error.accelerations[4], msg.feedback.error.accelerations[5]]

            # Normalize time in seconds
            # Compute new time
            _t_secs_feedback = msg.feedback.header.stamp.secs + msg.feedback.header.stamp.nsecs * 10**(-9)
            _t_secs_feedback = _t_secs_feedback - tinit_feedback

            data = [_t_secs_feedback] + \
                   _desired_pos + _desired_vel  + _desired_acc + \
                   _actual_pos + _actual_vel  + _actual_acc + \
                   _error_pos + _error_vel  + _error_acc
            data.insert(0, "/scaled_pos_joint_traj_controller/follow_joint_trajectory/feedback")
            if CSV_ON:
                csv_append(csv_file, data)
                # copy header in first line of tem csv
                csv_append(csv_temp, data)



pose_pos = delete_firstrow(pose_pos)
pose_quat = delete_firstrow(pose_quat)
pose_time = delete_firstrow(pose_time)
pose_time = pose_time.flatten()

real_pos = delete_firstrow(real_pos)
real_vel = delete_firstrow(real_vel)
real_eff = delete_firstrow(real_eff)
real_time = delete_firstrow(real_time)
real_time = real_time.flatten()

if CSV_ON:
    print("Parsed data. Saved as", csv_file)

# print(planned_pos.shape)
# print("planned pos \n", planned_pos)
# print(planned_vel.shape)
# print("planned pos \n", planned_vel)
# print(planned_acc.shape)
# print("planned acc \n", planned_acc)
# print(planned_time.shape)
# print("planned time \n", planned_time)

# print(real_pos.shape)
# print("real pos \n", real_pos)
# print(real_vel.shape)
# print("real pos \n", real_vel)
# print(real_eff.shape)
# print("real eff \n", real_eff)
# print(real_time.shape)
# print("real time \n", real_time)

# print(pose[:])
# print(pose_pos)
# fig = plt.figure(figsize=(2, 2))
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(pose_pos[:, 0], pose_pos[:, 1], pose_pos[:, 2])
# plt.show()


# ==== -------------------------------------------------------
# ===               POSITIONS XYZ
# ====
title = "Obstacle Avoidance Trajectories"

x = pose_pos[:, 0]
y = pose_pos[:, 1]
z = pose_pos[:, 2]
timegradient = pose_time

print("x", x)
print("y", y)
print("z", z)
print("time", pose_time)
print("len time:", len(timegradient))

figname = os.path.dirname(filename) + '/' + title + '.png'
ax3D, plt = urplot.scatter3d(x, y, z, timegradient)

plt.title(title, fontsize=20, color='black')
ax3D.set_xlabel('x (m)', fontsize=14, color='r')
ax3D.set_ylabel('y (m)', fontsize=14, color='g')
ax3D.set_zlabel('z (m)', fontsize=14, color='b')
plt.savefig(figname, format='png')

'''
# ==== -------------------------------------------------------
# ===               DETECT VARIATION IN SPEED IN Y
movement, indexes = urplot.vel_on_movement(y, pose_time, th=0.005)

fig = plt.figure()
ax = fig.add_subplot(111)
p2d = ax.scatter(pose_time, movement, s=30, marker='x')

start_indexes, end_indexes = urplot.startsends_movement(movement)

print(start_indexes)
print(pose_time[start_indexes])
print(pose_time[end_indexes])
p2d = ax.scatter(pose_time[start_indexes], movement[start_indexes], s=30, marker='o', color='green')
# ==== -------------------------------------------------------
# ===               DETECT VARIATION IN SPEED IN Y
movement, indexes = urplot.vel_on_movement(y, pose_time, th=0.005)



fig = plt.figure()
ax = fig.add_subplot(111)
p2d = ax.scatter(pose_time, movement, s=30, marker='x')

# ==== -------------------------------------------------------
# ===               DETECT INIT OF AVOID TRAJECTORY UNTIL END (not return)
yinit = -0.4
yend = 0.4
ydt = 0.01

tinit_avoid=[]
tinit_avoid_indexes=[]
tend_avoid_indexes=[]
tend_avoid=[]
for i in start_indexes:
    if abs(y[i] - yinit) < ydt:
        tinit_avoid.append(pose_time[i])
        tinit_avoid_indexes.append(i)

for i in end_indexes:
    if abs(y[i] - yend) < ydt:
        tend_avoid.append(pose_time[i + 1])  # TODO: Verify this is the good index
        tend_avoid_indexes.append(i + 1)
print("Avoid init times:  ", tinit_avoid)
print("Avoid end times:   ", tend_avoid)
print("Avoid init indexes from pose_time: ", tinit_avoid_indexes)
print("Avoid end indexes from pose_time: ", tend_avoid_indexes)
avoid_traj_time = np.array(tend_avoid) - np.array(tinit_avoid)
print("Total travel time: ", avoid_traj_time)

# ==== -------------------------------------------------------
# ===               PLOT 4 curves in 3 PLOTS
str_var = ["1-0", "1-1", "1-2"]
c = 0  # Counter of iteration
for i in range(3):
    title = "Obstacle Avoidance " + str_var[i]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for j in range(4):
        xx = x[tinit_avoid_indexes[c]: tend_avoid_indexes[c]]
        yy = y[tinit_avoid_indexes[c]: tend_avoid_indexes[c]]
        zz = z[tinit_avoid_indexes[c]: tend_avoid_indexes[c]]
        p2d = ax.scatter(xx, yy, zz, s=30, marker='o')
        c += 1  # Increase index counter
    plt.title(title, fontsize=20, color='black')
    ax.set_xlabel('x (m)', fontsize=14, color='r')
    ax.set_ylabel('y (m)', fontsize=14, color='g')
    ax.set_zlabel('z (m)', fontsize=14, color='b')
    ax.legend(['Moveit', 'PCHIP', 'Moveit URDRIVER', 'Moveit-PCHIP Speed'])

# ==== -------------------------------------------------------
# ===               PLOT 4 curves Q vs Temp in 3 PLOTS -2D

# *---- Get index of the same time values from above or close with real_time
tinit_avoid_real = []
tinit_avoid_real_indexes = []
tend_avoid_real_indexes = []
tend_avoid_real = []
for tt in tinit_avoid:
    diff_array = np.absolute(real_time - tt)
    index = diff_array.argmin()
    tti = real_time[index]
    tinit_avoid_real_indexes.append(index)
    tinit_avoid_real.append(tti)
for tt in tend_avoid:
    diff_array = np.absolute(real_time - tt)
    index = diff_array.argmin()
    tti = real_time[index]
    tend_avoid_real_indexes.append(index)
    tend_avoid_real.append(tti)
print("Avoid REAL init times:  ", tinit_avoid_real)
print("Avoid REAL end times:   ", tend_avoid_real)
print("Avoid REAL init indexes from pose_time: ", tinit_avoid_real_indexes)
print("Avoid REAL end indexes from pose_time: ", tend_avoid_real_indexes)
str_var = ["1-0", "1-1", "1-2"]
c = 0  # Counter of iteration
for i in range(3):
    title = "Obstacle Avoidance " + str_var[i]
    fig, axis = plt.subplots(3)
    fig.suptitle("Real Joint Trajectories " + title)
    for j in range(4):
        ind0 = tinit_avoid_real_indexes[c]
        indf = tend_avoid_real_indexes[c]
        q = real_pos[ind0:indf, :]
        qd = real_vel[ind0:indf, :]
        eff = real_eff[ind0:indf, :]
        timesteps = real_time[ind0:indf]

        m = q.shape[1]

        # Joint Position Plot
        axis[0].set_title("Position")
        axis[0].set(xlabel="Time", ylabel="Position")
        for ij in range(m):
            axis[0].plot(timesteps, q[:, ij])

        # Joint Velocity Plot
        axis[1].set_title("Velocity")
        axis[1].set(xlabel="Time", ylabel="Velocity")
        for ij in range(m):
            axis[1].plot(timesteps, qd[:, ij])

        # Joint Acceleration Plot
        axis[2].set_title("Effort")
        axis[2].set(xlabel="Time", ylabel="Effort")
        for ij in range(m):
            axis[2].plot(timesteps, eff[:, ij])

        # Legends
        legends = [f"Joint_{k + 1}" for k in range(m)]
        axis[0].legend(legends)
        axis[1].legend(legends)
        axis[2].legend(legends)

        c += 1  # Increase index counter
    # plt.title(title, fontsize=10, color='black')
    # ax.set_xlabel('x (m)', fontsize=14, color='r')
    # ax.set_ylabel('y (m)', fontsize=14, color='g')
    # ax.set_zlabel('z (m)', fontsize=14, color='b')
    ax.legend(['Moveit', 'PCHIP', 'Moveit URDRIVER', 'Moveit-PCHIP Speed'])
'''

# ==== -------------------------------------------------------
# ===               POSITIONS Q
# ====
title = "q positions"

x = real_time
q1 = real_pos[:, 0]
timegradient = real_time
print("len time:", len(timegradient))

figname = os.path.dirname(filename) + '/' + title + '.png'
ax, plt = urplot.scatter2d(x, q1, timegradient, colorsMap="jet")

plt.title(title, fontsize=20, color='black')
plt.ylabel('Position (rad)', fontsize=14, color='b')
plt.xlabel('time (s)', fontsize=14, color='b')
ax.legend(['q1'])
plt.savefig(figname, format='png')

print("Blocking")
plt.show()  # plt.show() to block

input("Continue... press enter")
print("Ending")
