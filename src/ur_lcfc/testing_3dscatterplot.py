import matplotlib.pyplot as plt
import matplotlib
import matplotlib.cm as cmx
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import urplot

# # Create Map
# cm = plt.get_cmap("RdYlGn")
# x = np.arange(2000)
# y = np.arange(2000) * 2
# z = np.arange(2000) * 0.5
# col = np.arange(2000)
# # # 2D Plot
# # fig = plt.figure()
# # ax = fig.add_subplot(111)
# # ax.scatter(x, y, s=10, c=col, marker='o')
# # # 3D Plot
# # fig = plt.figure()
# # ax3D = fig.add_subplot(111, projection='3d')
# # p3d = ax3D.scatter(x, y, z, s=30, c=col, marker='o') 
# # plt.show(block=False)


# # def scatter3d(x, y, z, cs=np.arange(30), colorsMap='jet', block=True):
# #     cm = plt.get_cmap(colorsMap)
# #     cNorm = matplotlib.colors.Normalize(vmin=min(cs), vmax=max(cs))
# #     scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)

# #     fig = plt.figure()
# #     ax3D = fig.add_subplot(111, projection='3d')
# #     p3d = ax3D.scatter(x, y, z, s=30, c=scalarMap.to_rgba(cs), marker='o')
# #     plt.show(block=block)

# urplot.scatter3d(x, y, z, col, colorsMap="RdYlGn", block=False)
# urplot.scatter3d(x, y, z, col, colorsMap="RdYlGn")
# urplot.scatter2d(x, y, col, colorsMap="jet", block=True)
# input("press enter to continue")


# MEAN VARIATION TEST

x = [0, 0, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.2, 0.3, 0.4, 0.6, 0.8, 0.11, 0.14, 0.15]
x = np.array(x)
x2 = np.flip(x)
x3 = np.concatenate((x, x2), axis=0)
print(x3)


# def mean_var_detection(arr, win=5, TH=1):
#     # win = 5  # data set moving window
#     n = len(arr)
#     r = TH
#     outa = np.zeros(n)
#     indexes = []
#     means = []
#     variances = []

#     fault1 = np.zeros(n)
#     fault2 = np.zeros(n)
#     u = []   # Decision based on value ui which follows a normal law N(0,1)
#     j = 0  # counter of iterations when entering the ui Decision
#     for i in np.arange(win, n):
#         suba = arr[i - win: i]
#         mni = np.mean(suba)
#         vari = np.var(suba)

#         print("i", i)
#         print("win", win)

#         if i == win:
#             mn_prev = mni
#         else:
#             mn_prev = means[j - 1]

#         ui = mni - mn_prev
#         ui = ui / (np.sqrt(vari) / win)

#         if abs(ui) > r:
#             j += 1
#             indexes.append(i)
#             fault1[i] = 1
#             fault2[i - int(np.ceil(win / 2))] = 1

#         means.append(mni)
#         variances.append(vari)
#         u.append(ui)

#     return means, variances, u, indexes, fault1, fault2


means, variances, zth, indexes, fault1, fault2 = urplot.mean_var_detection(x3, Z=2)

print('means \n', means)
print('variances \n', variances)
print('zth \n', zth)
print('indexes \n', indexes)
print('fault1 \n', fault1)
print('fault2 \n', fault2)
