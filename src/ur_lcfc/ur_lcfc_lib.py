import numpy as np
from numpy import linalg as LA
import math as m

def say_it_works():
    rospy.loginfo("Success! You just have imported a Python module in another package.")
class Traj:
    def velocities_from_waypoints(qm, dt):
        # Obtain the velocities from the q waypoints
        #  ~Approximate velocities in the segments given the timestep between points dt
        # ---------
        # Return
        #   qdm: q derivate matrix n x dof
        n = qm.shape[0]
        dof = qm.shape[1]
        qdm = np.zeros((0, dof))
        for i in range(n - 1):
            qd = (qm[i + 1, :] - qm[i - 1, :]) / dt
            qdm = np.vstack((qdm, qd))
        return qdm

    def times_at_cartesian_points(xyz, speed, tt, ST_WINDOW=0.5):
        '''
        Compute time restriction given speed
        It composes 3 sections
        1. First ST_WINDOW seconds from original times tt
        2. Middle phase where SPEED is taken into account
        3. Last ST_WINDOW seconds from original times tt

        xyz: Nx3 matrix [x y z] in m
        speed: mean linear speed m/s
        ttm original planned time for xyz
        ----------------------
        Return
         times at each point [Nx1 np.array]

        ST_WINDOW: SETTLING TIME WINDOW UP AND DOWN (START AND END)
        ST_WINDOW = 0.5  # SETTLING TIME OF 0.5s by default
        '''

        lenX = xyz.shape[0]
        dist_i = np.zeros((lenX, 1))
        tsi = np.zeros(lenX)

        # COPY FIRST 0.5 SECONDS OF TT
        UP_INDEX = 0
        DOWN_INDEX = 0

        tt_up = np.array([0])
        tt_down = np.array([0])
        #UP
        for i in range(1, lenX):
            if tt[i] < ST_WINDOW:
                tsi[i] = tt[i]
                tt_up = np.append(tt_up, tt[i])
            else:
                # tt_up = np.delete(tt_up, 0, 0)
                UP_INDEX = i
                break

        # COPY LAST 0.5 SECONDS OF TT
        # DOWN
        for j in range(lenX, 1, -1):
            i = j - 1  # range max is minus 1 of the len
            tend = tt[-1]
            ecart = tend - tt[i]
            if ecart < ST_WINDOW:
                tsi[i] = tt[i]
                tt_down = np.append(tt_down, tt[i])
            else:
                tt_down = np.flip(tt_down)
                tt_down = np.delete(tt_down, -1, 0)
                DOWN_INDEX = i + 1
                break

        # COMPUTE NEW
        for i in range(UP_INDEX, DOWN_INDEX):
            # start at position 1 instead of 0 to make the difference
            # Linear dist between data points
            di = LA.norm(xyz[i, :] - xyz[i - 1, :])
            dist_i[i][0] = di
            # Previous timestep +
            #                 time to travel the distance step at a given speed
            tsi[i] = tsi[i - 1] + di / speed

        print(tsi[DOWN_INDEX - 2 : DOWN_INDEX + 2])

        # CORRECTION OF LAST POINTS
        dt1 = tsi[DOWN_INDEX - 1] - tsi[DOWN_INDEX - 2]  # dt in the middle phase
        dt2 = tsi[DOWN_INDEX + 1] - tsi[DOWN_INDEX]  # dt in the end phase
        dtt = tsi[DOWN_INDEX] - tsi[DOWN_INDEX - 1]  # Difference between sections midle and down

        if dtt < 0:  # It means the original phase ended earlier
            tt_down = tt_down + abs(dtt) + dt1
        elif dtt > 0:  # It means the original time phase ended later
            tt_down = tt_down - abs(dtt) + dt2
        # Correct the time array
        len_ttdown = len(tt_down)
        tsi[-len_ttdown:] = tt_down

        print("dt1", dt1)
        print("dt2", dt2)
        print("dtt", dtt)
        print("tt_down", tt_down)

        tts = tsi

        # lenX = xyz.shape[0]
        # dist_i = np.zeros((lenX, 1))
        # tsi = [0]
        # for i in range(1, lenX):
        #     # start at position 1 instead of 0 to make the difference
        #     # Linear dist between data points
        #     di = LA.norm(xyz[i, :] - xyz[i - 1, :])
        #     dist_i[i][0] = di
        #     # Previous timestep +
        #     #                 time to travel the distance step at a given speed
        #     tsi.append(tsi[i - 1] + di / speed)
        # tts = np.array(tsi)

        return tts

    def times_at_wp(X, speed):
        # Compute time restriction given speed

        lenX = X.shape[1]
        dist_i = np.zeros((1, lenX))
        tsi = np.zeros((1, lenX))
        for i in range(1, lenX):
            # Linear dist between data points
            di = LA.norm(X[0:3, i] - X[0:3, i - 1])
            dist_i[0][i] = di
            # Previous timestep +
            #                 time to travel the distance step at a given speed
            tsi[0][i] = tsi[0][i - 1] + di / speed

        return tsi

    def quat_prod(q, r):
        # quatProd = product of quaternion q and r of arrays 1x4
        #  q = 1x4 array quaternion
        #  r = 1x4 array quaternion
        #  https://fr.mathworks.com/help/aerotbx/ug/quatmultiply.html
        q0 = q[0]; q1 = q[1]; q2 = q[2]; q3 = q[3];
        r0 = r[0]; r1 = r[1]; r2 = r[2]; r3 = r[3];

        n0 = r0 * q0 - r1 * q1 - r2 * q2 - r3 * q3
        n1 = r0 * q1 + r1 * q0 - r2 * q3 + r3 * q2
        n2 = r0 * q2 + r1 * q3 + r2 * q0 - r3 * q1
        n3 = r0 * q3 - r1 * q2 + r2 * q1 + r3 * q0

        quatp = np.array([n0, n1, n2, n3])
        return quatp

    def quat_conj(q):
        # quatConj returns the conjugate of quaternion q 1x4 array
        #   q = quaternion 1x4 array [a b c d]
        #   conjQ = conjugate of q 1x4 array [a -b -c -d]
        subq = np.array(q[1:len(q)])
        subq = subq * -1
        conjQ = np.concatenate(([q[0]], subq), axis=None)
        return conjQ

    def rotv2quat(rotV):
        # rotV2Quat RotateVector to Quaternion array 1x4 = [a b c d]
        #   rotV = angle*unitV
        ang = LA.norm(rotV)
        v = rotV / ang
        q1 = np.cos(ang / 2)
        q2 = np.sin(ang / 2) * v

        quat = np.concatenate((q1, q2), axis=None)
        return quat

    def rotate_point(P, alpha, axisVector):
        # rotatePoint  Rotate a point P along anaxis vector by alpha 
        # angle by using quaternions
        # [rP, quat] = rotatePoint ( P, alpha, axisVector)
        #   P = 1x3 array xyz
        #   alpha = angle in radians
        #   axsiVector = 1x3 array xyz
        #   See also rotV2Quat, quatConj, quatProd
        if alpha == 0:
            rP = P
            quat = [0, P]
            return rP, quat

        # Compute rotation vector angle*unitVector
        unitV = axisVector / LA.norm(axisVector)
        rotV = alpha * unitV  #  Normalized, then it is a Rotation Vector)

        # Extracte/compute quaternion given a rotV
        quat = Traj.rotv2quat(rotV)  # rotV 2 Quaternion array 1x4
        #  quat = Quaternion(quat) # form of Peter Corke s << i j k >>
        quatbar = Traj.quat_conj(quat)

        # create quat q = (0, vx, vy, vz)
        quat_P = np.concatenate(([0], P), axis=None)
        quat_Pp0 = Traj.quat_prod(quat, quat_P)
        quat_Pp = Traj.quat_prod(quat_Pp0, quatbar)
        #  quat_Pp = quat*quat_P*quatbar  # ===> q*a*q'
        #  [ang,Pp(1),Pp(2),Pp(3)] = parts(quat_Pp) #  Extract P vector 1x3 P'
        # quat_Pp_vec = double(quat_Pp) #  Convert to vector 1x4
        quat_Pp_vec = quat_Pp  # Convert to vector 1x4
        Pp = quat_Pp_vec[1:]  # Extract Point vector 1x3 P'
        rP = Pp  # Rotated point

        return rP, quat

    def rotate_points(X, alpha, axisVector):
        # rotatePoint  Rotate a point P along anaxis vector by alpha 
        # angle by using quaternions
        # [rP, quat] = rotatePoint ( P, alpha, axisVector)
        #   P = 1x3 array xyz
        #   alpha = angle in radians
        #   axsiVector = 1x3 array xyz
        #   See also rotV2Quat, quatConj, quatProd

        X1 = X[0:3, 0]
        # Array with respect from its own coordinate system
        Xo = np.zeros(X.shape)
        Xo[0, :] = X[0, :] - X1[0]
        Xo[1, :] = X[1, :] - X1[1]
        Xo[2, :] = X[2, :] - X1[2]
        lenX = X.shape[1]
        Xr = X + 0.0
        for i in range(lenX):
            Xai, _ = Traj.rotate_point(Xo[0:3, i], alpha, axisVector)
            Xr[0:3, i] = Xai + X1
            # %Array with respect from its own coordinate system
        return Xr



        return rP, quat

    def rotate_pointZYX(P, gammaZ, betaY, alphaX):
        # rotatePointZYX  rotates a point P by the angles gamma, beta, alpha
        #                 in the axis order ZYX, in a consecutive manner taking
        #                 into accounnt the Positive/Negative direct of right
        #                 handed direction
        # [Pr, XYZpr] = rotatePointZYX(P,gammaZ,betaY,alphaX)
        #       P = Point 1x3 vector
        #   gamma = angle to rotate around axis Z
        #   beta  = angle to rotate around axis Y
        #   alpha = angle to rotate around axis X
        #      Pr = Rotated point 1x3 vector
        #    XYZpr = Unit coordinate system [xVec;yVec;zVec]
        #
        #       See also rotatePoint

        P1, q1 = Traj.rotate_point(P, gammaZ, [0, 0, 1])

        X1, qx = Traj.rotate_point([1, 0, 0], gammaZ, [0, 0, 1])
        Y1 = np.cross([0, 0, 1], X1)
        # Z1 = [0, 0, 1]

        X2, qx = Traj.rotate_point(X1, betaY, Y1)
        Y2 = Y1
        # Z2 = np.cross(X2, Y2)

        X3 = X2
        Y3, qx = Traj.rotate_point(Y2, alphaX, X2)
        Z3 = np.cross(X3, Y2)

        P2, q2 = Traj.rotate_point(P1, betaY, Y1)
        Pr, q3 = Traj.rotate_point(P2, alphaX, X2)

        XYZpr = [X3, Y3, Z3]

        return Pr  # , *XYZpr

    def rotate_points_ZYX(X, gammaZ, betaY, alphaX, *translationVec):
        '''
        X = 6xN : [P;w]
        '''
        if len(translationVec) == 0:
            translationVec = [0, 0, 0]

        lenX = X.shape[1]
        Xr = X + 0.0
        for i in range(lenX):
            Xai = Traj.rotate_pointZYX(X[0:3, i], gammaZ, -betaY, 0)
            # O are the coordinates of the obstacle
            Xr[0:3, i] = Xai + translationVec

        return Xr

    def vec_angle360(v1, v2, n):
        # vecAangle360(v1,v2,n)Modified to get the positive angles and
        #       now in radians instead of degrees
        # #  v1 = 1x3 vector
        # #  v2 = 1x3 vector
        # #  n = vector 1x3 normal to v1 v2
        # #  https://fr.mathworks.com/matlabcentral/answers/501449-angle-betwen-two-3d-vectors-in-the-range-0-360-degree
        # #  #Example
        # #   phi = vecangle360(u,v, [0 1 0]);
        # #  See also atan2
        x = np.cross(v1, v2)
        c = np.sign(np.dot(x, n)) * LA.norm(x)
        a = np.arctan2(c, np.dot(v1, v2))

        return a

    def get_angles_zy(Vec):
        # getAnglesZY   given a Vector 1x3 obtain the rotation angles around ZY
        #               having the new Vector as the direction of X'
        #   getAnglesZY(Vec)
        #   Vec = 1x3 vec [x y z]
        #   See also vecAngle360
        #   USES Numpy and linalg
        vMag = LA.norm(Vec)
        Vi = Vec + 0  # WTF add 0 TO STOP CHANGING the Orig Vec var
        Vi[2] = 0  # Make 0 the z value to work only in XY

        # in plane XY with Z as normal
        gamma = Traj.vec_angle360([1, 0, 0],
                                  Vi,
                                  [0, 0, 1])
        beta = np.arcsin(Vec[2] / vMag)
        #  alpha = atan(Vec(3)/Vec(1))

        return gamma, beta

    def arc_path(R, arcAngles, translationVec, samples):
        # arcPath creats an arc wth radius R, given two polar ang arcAngles 1x2
        # with n samples, able to change the center origin of the arc wit
        # translationVec
        #   R = radius
        #   arcAngles = [ theta1, theta2 ], 1x2 vector
        #   translationVec = [xt yt zt], 1x3 vector translation of origin to
        #                    another location
        #   samples = N, number of samples
        #   See also linspace

        # samples = 50
        if translationVec == 0:
            translationVec = [0, 0, 0]
        # Setting the angle samples
        th = np.linspace(arcAngles[0], arcAngles[1], samples)

        # Original first circle in line x-axis with R
        t = translationVec
        yc = th * 0 + t[1]
        zc = np.sin(th) * R + t[2]
        xc = np.cos(th) * R + t[0]

        return xc, yc, zc

    def arc_avoid(x, yo, R, h, R2, R3):
        # Samples of R1 R2 R3 Arc given a secnt = 2R and height h from obs
        # Number of samples of given arc
        # Warning : It only works at the moment with the same given orientation
        # at x0 y xf states
        n2 = 5   # left ard up
        n1 = 10  # main arc
        n3 = 5   # right arc down

        xxj = x.shape[1]  # Input are x0 and xF poses (From point A B poses)
        # We must compute the whole x
        if xxj == 2:
            x0 = np.array([x[:, 0]])
            xf = np.array([x[:, 1]])
            st = 0.01
            # s=0: st :1
            s = np.arange(0, 1 + st, st)

            str = xf[0][1] - x0[0][1]
            str = str / (s.size - 1)

            lens = s.size
            # xp = np.zeros((1, lens))
            xp = np.zeros((1, lens))
            yp = np.zeros((1, lens))
            zp = np.zeros((1, lens))
            rollp = np.zeros((1, lens))
            pitchp = np.zeros((1, lens))
            yawp = np.zeros((1, lens))

            for i in range(lens):
                #  creating the original xyz ranges arrays
                xp[0][i] = (1 - s[i]) * x0[0][0] + s[i] * xf[0][0]
                yp[0][i] = (1 - s[i]) * x0[0][1] + s[i] * xf[0][1]
                zp[0][i] = (1 - s[i]) * x0[0][2] + s[i] * xf[0][2]

                rollp[0][i] = (1 - s[i]) * x0[0][3] + s[i] * xf[0][3]
                pitchp[0][i] = (1 - s[i]) * x0[0][4] + s[i] * xf[0][4]
                yawp[0][i] = (1 - s[i]) * x0[0][5] + s[i] * xf[0][5]
            x = np.concatenate((xp, yp, zp, rollp, pitchp, yawp), axis=0)
        else:
            xp = np.array([x[0, :]])
            yp = np.array([x[1, :]])
            zp = np.array([x[2, :]])

            rollp = np.array([x[3, :]])
            pitchp = np.array([x[4, :]])
            yawp = np.array([x[5, :]])

            # x = np.concatenate((xp, yp, zp, np.zeros((3, lens))), axis=0)

            x0 = np.array([x[:, 0]])
            xf = np.array([x[:, x0.size - 1]])

        # Equation of the lines
        mLz = (xf[0][2] - x0[0][2]) / (xf[0][1] - x0[0][1])
        bLz = x0[0][2] - mLz * x0[0][1]  # b = y - mx (subs y by z) b = z -mx

        mLy = (xf[0][1] - x0[0][1]) / (xf[0][0] - x0[0][0])
        bLy = x0[0][1] - mLy * x0[0][0]  # b = y - mx (subs y by z) b = z -mx

        mLx = (xf[0][0] - x0[0][0]) / (xf[0][1] - x0[0][1])
        bLx = x0[0][0] - mLx * x0[0][1]  # b = x - my (subs y by z) b = z -mx

        # # Point obstacle
        # Project to line given an x
        # yo= -0.1                             #   <<<<==========
        xo = mLx * yo + bLx
        zo = mLz * yo + bLz

        obstacle = [xo, yo, zo]

        # # R1 R2 R3 Arc given a secant = 2R and height h from obstacle
        # Number of samples of given arc
        # n2 = 5 n1 = 10 n3 = 5

        r = R
        #  h = 0.6*R # h goes from 0 to 1

        Rs = 1 / (2 * h) * (h**2 + r**2)
        z_s = Rs - h

        # Rs2 = Rs + R2
        bx2 = m.sqrt((Rs + R2)**2 - (R2 + z_s)**2)
        beta2 = m.acos((R2 + z_s) / (Rs + R2))

        # Rs3 = Rs + R3
        bx3 = m.sqrt((Rs + R3)**2 - (R3 + z_s)**2)
        beta3 = m.acos((R3 + z_s) / (Rs + R3))

        Os = [0, 0, -z_s]
        O2 = [-bx2, 0, R2]
        O3 = [bx3, 0, R3]

        arc_s = [m.pi / 2 + beta2, m.pi / 2 - beta3]
        arc2 = [-m.pi / 2, - m.pi / 2 + beta2]
        arc3 = [-m.pi / 2 - beta3, - m.pi / 2]

        xs2, ys2, zs2 = Traj.arc_path(R2, arc2, O2, n2)
        xs, ys, zs = Traj.arc_path(Rs, arc_s, Os, n1)
        xs3, ys3, zs3 = Traj.arc_path(R3, arc3, O3, n3)

        # Concat vectors X Y Z of avoid traj arcs => Avoid Sub Trajs
        # With the SECANTS computation
        xss = np.concatenate((xs2[0:-1], xs, xs3[1:]), axis=None)
        yss = np.concatenate((ys2[:-1], ys, ys3[1:]), axis=None)
        zss = np.concatenate((zs2[0:-1], zs, zs3[1:]), axis=None)

        # TODO: Take into account the final Orientation (gradual movement along traj, now it is fixed to first imput Orientation)
        wroll = np.ones((1, xss.size)) * rollp[0][0]
        wpitch = np.ones((1, xss.size)) * pitchp[0][0]
        wyaw = np.ones((1, xss.size)) * yawp[0][0]
        wss = np.concatenate((wroll, wpitch, wyaw), axis=0)
        XsAv = np.concatenate(([xss], [yss], [zss], wss), axis=0)

        #  "Cut" original trajectory by the length of Avoid Sub Traj
        # Obtain the indexes of the original trajectory where the
        # dist matches to Avoid Sub Traj distance, to later concatenate all
        # trajectories in the corrected original trajectory

        avoidInit_obs = abs(xss[0])  # Dist from obst to the Beg of Avoid
        avoidEnd_obs = abs(xss[-1])  # Dist from obst to the End of Avoid
        # lenCut = xss[-1] - xss[0]  # From the X axis what is the length

        lenxp = xp.size
        yoIndex = np.argwhere(xp > yo)  # find(xp > yo)
        yoIndex = yoIndex[0][1]
        limit2RIGHT = False
        limit2LEFT = False
        # Going from Left2Right to find where the dist matches avoidInit_obs
        for i in range(lenxp):
            xpi = xp[0][i]
            ypi = yp[0][i]
            zpi = zp[0][i]
            obs_pi = np.sqrt((xo - xpi)**2 + (yo - ypi)**2 + (zo - zpi)**2)
            if obs_pi < avoidInit_obs:
                avoidInit_index = i - 1  # ===== THIS INDEX
                break
        # Going from RIGHT2LEFT to find whr distance matches the avoidEnd_obs
        arr = range(lenxp)
        arr = arr[::-1]  # inverse the array to go from Right2Left
        for i in arr:
            xpi = xp[0][i]
            ypi = yp[0][i]
            zpi = zp[0][i]
            obs_pi = np.sqrt((xo - xpi)**2 + (yo - ypi)**2 + (zo - zpi)**2)
            if obs_pi < avoidEnd_obs:
                avoidEnd_index = i + 1   # ===== THIS INDEX
                break

        # Get the vector frm orig traj given AvoidInit and AvoidEnd indexes
        if avoidInit_index < 0:
            limit2LEFT = True
            print('Avoid traj starting out of Xp starting limits')
            avoidInit_Point = [xp[0][0], yp[0][0], zp[0][0]]
            avoidInit_index = -1
        else:
            avoidInit_Point = np.array([xp[0][avoidInit_index],
                                        yp[0][avoidInit_index],
                                        zp[0][avoidInit_index]])

        if avoidEnd_index >= xp.size:
            limit2RIGHT = True
            print('Avoid traj ending out of Xp starting limits')
            avoidEnd_Point = [xp[0][-1], yp[0][-1], zp[0][-1]]
            # avoidEnd_index = 9999
        else:
            avoidEnd_Point = np.array([xp[0][avoidEnd_index],
                                       yp[0][avoidEnd_index],
                                       zp[0][avoidEnd_index]])

        avoidVec = avoidEnd_Point - avoidInit_Point

        # # Get anglez arund ZY from Vec (X' direction) AND %
        # Given angles ZY rotate Point

        gammaZ, betaY = Traj.get_angles_zy(avoidVec)

        # Rotate full array of points in the new X' and then translating the
        # rotations point where the obstacle is
        lenXav = XsAv.shape[1]
        # Xav = np.zeros((6, lenXav))
        Xav = XsAv + 0.0 # Avoid modifiying original XsAv

        for i in range(lenXav):
            Xai = Traj.rotate_pointZYX(XsAv[0:3, i], gammaZ, -betaY, 0)
            # O are the coordinates of the obstacle
            Xav[0:3, i] = Xai + obstacle

        # NOT IMPLEMENTED IN PYTHON
        # Obtain the unit XYZpr coordinate system
        # [~,XYZpr] = Traj.rotate_pointZYX(XsAv(1:3,1)', gammaZ, -betaY, 0);

        # Concat Orig Traj with Xav

        xss = np.concatenate((xs2[0:-1], xs, xs3[1:]), axis=None)

        if limit2LEFT:
            print('LEFT LIMIT Avoid traj STARTING out of Xp starting limits')
            subx = x[:, avoidEnd_index:]
            XnAv = np.concatenate((Xav, subx), axis=1)
            XnAv_minimal = np.concatenate((Xav, xf.T), axis=1)
        elif limit2RIGHT:
            print('RIGHT LIMIT Avoid traj ENDING out of Xp starting limits')
            XnAv = np.concatenate((x[:, 0:avoidInit_index + 1], Xav), axis=1)
            XnAv_minimal = np.concatenate((x0.T, Xav), axis=1)
        else:
            subx = x[:, avoidEnd_index:]
            XnAv = np.concatenate((x[:, 0:avoidInit_index + 1], Xav, subx), axis=1)
            XnAv_minimal = np.concatenate((x0.T, Xav, xf.T), axis=1)

        # XavT1 = Traj.rotate_pointZYX(XsAv[0:3, 2], gammaZ, -betaY, 0)
        # XavT = XavT1 + obstacle  # Test
        # XavTt = np.transpose([XavT])
        # t2 = np.transpose([1, 2, 3])
        # t2 = [4, 5, 6]
        # Xav[0:3, 2] = XavT

        # return gammaZ, betaY, 0
        # return XavT, XavTt, t2, Xav
        # return avoidInit_Point, avoidEnd_Point, avoidVec

        return XnAv, Xav, XnAv_minimal, obstacle,  # XsAv, obstacle

    def arc_avoid_min(x, yo, R, h, R2, R3):
        # Samples of R1 R2 R3 Arc given a secnt = 2R and height h from obs
        # Number of samples of given arc
        n2 = 5   # left ard up
        n1 = 10  # main arc
        n3 = 5   # right arc down

        xxj = x.shape[1]  # Input are x0 and xF poses (From point A B poses)
        # We must compute the whole x

        # UPDATE exchanged xy yp to change x y axis to match the UR10 config
        xp = np.array([x[0, :]])
        yp = np.array([x[1, :]])
        zp = np.array([x[2, :]])

        rollp = np.array([x[3, :]])
        pitchp = np.array([x[4, :]])
        yawp = np.array([x[5, :]])

        x0 = np.array([x[:, 0]])
        xf = np.array([x[:, 1]])

        # Equation of the lines
        mLz = (xf[0][2] - x0[0][2]) / (xf[0][1] - x0[0][1])
        bLz = x0[0][2] - mLz * x0[0][1]  # b = y - mx (subs y by z) b = z -mx

        mLy = (xf[0][1] - x0[0][1]) / (xf[0][0] - x0[0][0])
        bLy = x0[0][1] - mLy * x0[0][0]  # b = y - mx (subs y by z) b = z -mx

        mLx = (xf[0][0] - x0[0][0]) / (xf[0][1] - x0[0][1])
        bLx = x0[0][0] - mLx * x0[0][1]  # b = x - my (subs y by z) b = z -mx

        # # Point obstacle
        # Project to line given an x
        # yo= -0.1                             #   <<<<==========
        xo = mLx * yo + bLx
        zo = mLz * yo + bLz

        obstacle = [xo, yo, zo]

        # # R1 R2 R3 Arc given a secant = 2R and height h from obstacle
        # Number of samples of given arc
        # n2 = 5 n1 = 10 n3 = 5

        r = R
        #  h = 0.6*R # h goes from 0 to 1

        Rs = 1 / (2 * h) * (h**2 + r**2)
        z_s = Rs - h

        # Rs2 = Rs + R2
        bx2 = m.sqrt((Rs + R2)**2 - (R2 + z_s)**2)
        beta2 = m.acos((R2 + z_s) / (Rs + R2))

        # Rs3 = Rs + R3
        bx3 = m.sqrt((Rs + R3)**2 - (R3 + z_s)**2)
        beta3 = m.acos((R3 + z_s) / (Rs + R3))

        Os = [0, 0, -z_s]
        O2 = [-bx2, 0, R2]
        O3 = [bx3, 0, R3]

        arc_s = [m.pi / 2 + beta2, m.pi / 2 - beta3]
        arc2 = [-m.pi / 2, - m.pi / 2 + beta2]
        arc3 = [-m.pi / 2 - beta3, - m.pi / 2]

        xs2, ys2, zs2 = Traj.arc_path(R2, arc2, O2, n2)
        xs, ys, zs = Traj.arc_path(Rs, arc_s, Os, n1)
        xs3, ys3, zs3 = Traj.arc_path(R3, arc3, O3, n3)

        # Concat vectors X Y Z of avoid traj arcs => Avoid Sub Trajs
        # With the SECANTS computation
        xss = np.concatenate((xs2[0:-1], xs, xs3[1:]), axis=None)
        yss = np.concatenate((ys2[:-1], ys, ys3[1:]), axis=None)
        zss = np.concatenate((zs2[0:-1], zs, zs3[1:]), axis=None)

        # TODO: Take into account the final Orientation (gradual movement along traj, now it is fixed to first imput Orientation)
        wroll = np.ones((1, xss.size)) * rollp[0][0]
        wpitch = np.ones((1, xss.size)) * pitchp[0][0]
        wyaw = np.ones((1, xss.size)) * yawp[0][0]
        wss = np.concatenate((wroll, wpitch, wyaw), axis=0)
        XsAv = np.concatenate(([xss], [yss], [zss], wss), axis=0)

        #  "Cut" original trajectory by the length of Avoid Sub Traj
        # Obtain the indexes of the original trajectory where the
        # dist matches to Avoid Sub Traj distance, to later concatenate all
        # trajectories in the corrected original trajectory

        avoidInit_obs = abs(xss[0])  # Dist from obst to the Beg of Avoid
        avoidEnd_obs = abs(xss[-1])  # Dist from obst to the End of Avoid
        # lenCut = xss[-1] - xss[0]  # From the X axis what is the length

        lenxp = xp.size
        yoIndex = np.argwhere(xp > yo)  # find(xp > yo)
        yoIndex = yoIndex[0][1]
        limit2RIGHT = False
        limit2LEFT = False
        # Going from Left2Right to find where the dist matches avoidInit_obs
        xpi = xp[0][0]
        ypi = yp[0][0]
        zpi = zp[0][0]
        obs_pi = np.sqrt((yo - xpi)**2 + (yo - ypi)**2 + (zo - zpi)**2)
        # print(obs_pi)
        if obs_pi < avoidInit_obs:
            avoidInit_index = -1  # ===== THIS INDEX
            limit2LEFT = True
        # Going from RIGHT2LEFT to find whr distance matches the avoidEnd_obs
        xpi = xp[0][1]
        ypi = yp[0][1]
        zpi = zp[0][1]
        obs_pi = np.sqrt((xo - xpi)**2 + (yo - ypi)**2 + (zo - zpi)**2)
        if obs_pi < avoidEnd_obs:
            avoidEnd_index = 1  # ===== THIS INDEX
            limit2RIGHT = True
        avoidInit_Point = np.array([xp[0][0],
                                    yp[0][0],
                                    zp[0][0]])

        avoidEnd_Point = np.array([xp[0][1],
                                   yp[0][1],
                                   zp[0][1]])

        avoidVec = avoidEnd_Point - avoidInit_Point

        # # Get anglez arund ZY from Vec (X' direction) AND %#Given
        # angles ZY rotate Point
        gammaZ, betaY = Traj.get_angles_zy(avoidVec)

        # Rotate full array of points in the new X' and then translating
        #  the rotations point where the obstacle is
        lenXav = XsAv.shape[1]
        Xav = np.zeros((6, lenXav))

        Xav = Traj.rotate_points_ZYX(XsAv, gammaZ, betaY, 0, obstacle)


        # for i in range(lenXav):
        #     Xai = Traj.rotate_pointZYX(XsAv[0:3, i], gammaZ, -betaY, 0)
        #     # O are the coordinates of the obstacle
        #     Xav[0:3, i] = Xai + obstacle

        # NOT IMPLEMENTED IN PYTHON
        # Obtain the unit XYZpr coordinate system
        # [~,XYZpr] = Traj.rotate_pointZYX(XsAv(1:3,1)', gammaZ, -betaY, 0);

        # Concat Orig Traj with Xav

        xss = np.concatenate((xs2[0:-1], xs, xs3[1:]), axis=None)

        if limit2LEFT:
            print('LEFT LIMIT Avoid traj STARTING out of Xp starting limits')
            print('input [x, yo, R, h, R2, R3]')
            print([x, yo, R, h, R2, R3])
            XnAv_minimal = np.concatenate((Xav, xf.T), axis=1)
        elif limit2RIGHT:
            print('RIGHT LIMIT Avoid traj ENDING out of Xp starting limits')
            print('input [x, yo, R, h, R2, R3]')
            print([x, yo, R, h, R2, R3])
            XnAv_minimal = np.concatenate((x0.T, Xav), axis=1)
        else:

            XnAv_minimal = np.concatenate((x0.T, Xav, xf.T), axis=1)

        # XavT1 = Traj.rotate_pointZYX(XsAv[0:3, 2], gammaZ, -betaY, 0)
        # XavT = XavT1 + obstacle  # Test
        # XavTt = np.transpose([XavT])
        # t2 = np.transpose([1, 2, 3])
        # t2 = [4, 5, 6]
        # Xav[0:3, 2] = XavT

        # return gammaZ, betaY, 0 
        # return XavT, XavTt, t2, Xav
        # return avoidInit_Point, avoidEnd_Point, avoidVec

        return XnAv_minimal, Xav, XsAv, obstacle  # XsAv, obstacle

    def arc_avoid_min_epi(x, yo, R, h, R2, *R3):
        # Samples of R1 R2 R3 Arc given a secnt = 2R and height h from obs
        # Number of samples of given arc
        n2 = 5   # left ard up
        n1 = 10  # main arc
        n3 = 5   # right arc down

        xxj = x.shape[1]  # Input are x0 and xF poses (From point A B poses)
        # We must compute the whole x

        xp = np.array([x[0, :]])
        yp = np.array([x[1, :]])
        zp = np.array([x[2, :]])

        rollp = np.array([x[3, :]])
        pitchp = np.array([x[4, :]])
        yawp = np.array([x[5, :]])


        x0 = np.array([x[:, 0]])
        xf = np.array([x[:, 1]])

        # Equation of the lines
        mLz = (xf[0][2] - x0[0][2]) / (xf[0][1] - x0[0][1])
        bLz = x0[0][2] - mLz * x0[0][1]  # b = y - mx (subs y by z) b = z -mx

        mLy = (xf[0][1] - x0[0][1]) / (xf[0][0] - x0[0][0])
        bLy = x0[0][1] - mLy * x0[0][0]  # b = y - mx (subs y by z) b = z -mx

        mLx = (xf[0][0] - x0[0][0]) / (xf[0][1] - x0[0][1])
        bLx = x0[0][0] - mLx * x0[0][1]  # b = x - my (subs y by z) b = z -mx

        # # Point obstacle
        # Project to line given an x
        # yo= -0.1                             #   <<<<==========
        xo = mLx * yo + bLx
        zo = mLz * yo + bLz

        obstacle = [xo, yo, zo]

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
        # # # # #           ARCS  R1 R2 R3                  # # # # #

        # # R1 R2 R3 Arc given a secant = 2R and height h from obstacle
        # Number of samples of given arc
        # n2 = 5 n1 = 10 n3 = 5

        r = R
        #  h = 0.6*R # h goes from 0 to 1

        Rs = 1 / (2 * h) * (h**2 + r**2)
        z_s = Rs - h

        # Rs2 = Rs + R2
        bx2 = m.sqrt((Rs + R2)**2 - (R2 + z_s)**2)
        beta2 = m.acos((R2 + z_s) / (Rs + R2))

        # Rs3 = Rs + R3
        # bx3 = m.sqrt((Rs + R3)**2 - (R3 + z_s)**2)
        # beta3 = m.acos((R3 + z_s) / (Rs + R3))
        # bx3 calculation given no R3 to calculate R3 = Obst -> Point B 
        xpi = xp[0][1]
        ypi = yp[0][1]
        zpi = zp[0][1]

        bx3 = np.sqrt((xo - xpi)**2 + (yo - ypi)**2 + (zo - zpi)**2)
        R3 = (bx3**2 - Rs**2 + z_s**2) / (2 * Rs - 2 * z_s)
        beta3 = m.acos((R3 + z_s) / (Rs + R3))

        Os = [0, 0, -z_s]
        O2 = [-bx2, 0, R2]
        O3 = [bx3, 0, R3]

        arc_s = [m.pi / 2 + beta2, m.pi / 2 - beta3]
        arc2 = [-m.pi / 2, - m.pi / 2 + beta2]
        arc3 = [-m.pi / 2 - beta3, - m.pi / 2]

        xs2, ys2, zs2 = Traj.arc_path(R2, arc2, O2, n2)
        xs, ys, zs = Traj.arc_path(Rs, arc_s, Os, n1)
        xs3, ys3, zs3 = Traj.arc_path(R3, arc3, O3, n3)

        # Concat vectors X Y Z of avoid traj arcs => Avoid Sub Trajs
        # With the SECANTS computation
        xss = np.concatenate((xs2[0:-1], xs, xs3[1:]), axis=None)
        yss = np.concatenate((ys2[:-1], ys, ys3[1:]), axis=None)
        zss = np.concatenate((zs2[0:-1], zs, zs3[1:]), axis=None)

        # TODO: Take into account the final Orientation (gradual movement along traj, now it is fixed to first imput Orientation)
        wroll = np.ones((1, xss.size)) * rollp[0][0]
        wpitch = np.ones((1, xss.size)) * pitchp[0][0]
        wyaw = np.ones((1, xss.size)) * yawp[0][0]
        wss = np.concatenate((wroll, wpitch, wyaw), axis=0)
        XsAv = np.concatenate(([xss], [yss], [zss], wss), axis=0)

        #  "Cut" original trajectory by the length of Avoid Sub Traj
        # Obtain the indexes of the original trajectory where the
        # dist matches to Avoid Sub Traj distance, to later concatenate all
        # trajectories in the corrected original trajectory

        avoidInit_obs = abs(xss[0])  # Dist from obst to the Beg of Avoid
        avoidEnd_obs = abs(xss[-1])  # Dist from obst to the End of Avoid
        # lenCut = xss[-1] - xss[0]  # From the X axis what is the length

        lenxp = xp.size
        yoIndex = np.argwhere(xp > yo)  # find(xp > yo)
        yoIndex = yoIndex[0][1]
        limit2RIGHT = False
        limit2LEFT = False
        # Going from Left2Right to find where the dist matches avoidInit_obs
        xpi = xp[0][0]
        ypi = yp[0][0]
        zpi = zp[0][0]
        obs_pi = np.sqrt((xo - xpi)**2 + (yo - ypi)**2 + (zo - zpi)**2)
        # print(obs_pi)
        if obs_pi < avoidInit_obs:
            avoidInit_index = -1  # ===== THIS INDEX
            limit2LEFT = True
        # Going from RIGHT2LEFT to find whr distance matches the avoidEnd_obs
        xpi = xp[0][1]
        ypi = yp[0][1]
        zpi = zp[0][1]
        obs_pi = np.sqrt((xo - xpi)**2 + (yo - ypi)**2 + (zo - zpi)**2)
        if obs_pi < avoidEnd_obs:
            avoidEnd_index = 1  # ===== THIS INDEX
            limit2RIGHT = True
        avoidInit_Point = np.array([xp[0][0],
                                    yp[0][0],
                                    zp[0][0]])

        avoidEnd_Point = np.array([xp[0][1],
                                   yp[0][1],
                                   zp[0][1]])

        avoidVec = avoidEnd_Point - avoidInit_Point

        # # Get anglez arund ZY from Vec (X' direction) AND %#Given
        # angles ZY rotate Point
        gammaZ, betaY = Traj.get_angles_zy(avoidVec)

        # Rotate full array of points in the new X' and then translating
        #  the rotations point where the obstacle is
        lenXav = XsAv.shape[1]
        Xav = np.zeros((6, lenXav))

        for i in range(lenXav):
            Xai = Traj.rotate_pointZYX(XsAv[0:3, i], gammaZ, -betaY, 0)
            # O are the coordinates of the obstacle
            Xav[0:3, i] = Xai + obstacle

        # NOT IMPLEMENTED IN PYTHON
        # Obtain the unit XYZpr coordinate system
        # [~,XYZpr] = Traj.rotate_pointZYX(XsAv(1:3,1)', gammaZ, -betaY, 0);

        # Concat Orig Traj with Xav

        xss = np.concatenate((xs2[0:-1], xs, xs3[1:]), axis=None)

        if limit2LEFT:
            print('LEFT LIMIT Avoid traj STARTING out of Xp starting limits')
            XnAv_minimal = np.concatenate((Xav, xf.T), axis=1)
        elif limit2RIGHT:
            print('RIGHT LIMIT Avoid traj ENDING out of Xp starting limits')
            XnAv_minimal = np.concatenate((x0.T, Xav), axis=1)
        else:

            XnAv_minimal = np.concatenate((x0.T, Xav, xf.T), axis=1)

        # XavT1 = Traj.rotate_pointZYX(XsAv[0:3, 2], gammaZ, -betaY, 0)
        # XavT = XavT1 + obstacle  # Test
        # XavTt = np.transpose([XavT])
        # t2 = np.transpose([1, 2, 3])
        # t2 = [4, 5, 6]
        # Xav[0:3, 2] = XavT

        # return gammaZ, betaY, 0 
        # return XavT, XavTt, t2, Xav
        # return avoidInit_Point, avoidEnd_Point, avoidVec

        return XnAv_minimal, Xav, XsAv, obstacle  # XsAv, obstacle
