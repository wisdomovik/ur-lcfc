"""
Visualize the transformations
Matplotlib:
quiver plot
"""

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import matplotlib.cm as cmx


# Function to plot a single transformation
def plot_transformation(transformation):
    """
    Plot Transformation matrix
    ...
    Parameters
    ---
    transformation: 4x4 transformation matrix
    Returns
    ---
    None
    Notes
    ---
    RGB -> XYZ
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # x, y, z of 6 arrows in a quiver plot
    x = np.array([0, 0, 0, transformation[0, 3], transformation[0, 3], transformation[0, 3]])
    y = np.array([0, 0, 0, transformation[1, 3], transformation[1, 3], transformation[1, 3]])
    z = np.array([0, 0, 0, transformation[2, 3], transformation[2, 3], transformation[2, 3]])

    # u, v, w of 6 arrows in a quiver plot
    u = np.concatenate([np.array([1, 0, 0]), transformation[:3, 0]])
    v = np.concatenate([np.array([0, 1, 0]), transformation[:3, 1]])
    w = np.concatenate([np.array([0, 0, 1]), transformation[:3, 2]])

    # Color(RGB) for 6 arrows, original X, Y, Z and then transformed X, Y, Z
    red = np.array([1, 0, 0])
    green = np.array([0, 1, 0])
    blue = np.array([0, 0, 1])
    colors = np.array([red, green, blue, red, green, blue])

    q = ax.quiver(x, y, z, u, v, w, length=0.05, colors = colors, lw=1)

    plt.plot([x[0], x[3]], [y[0], y[3]], [z[0], z[3]], '--', color = 'black')

    plt.show()


# Function to plot a list of transformations
def plot_transformations(transformations):
    """
    Plot Transformation matrix
    ...
    Parameters
    ---
    transformation: list of 4x4 transformation matrix
    Returns
    ---
    None
    Notes
    ---
    RGB -> XYZ
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    x = np.array([])
    y = np.array([])
    z = np.array([])

    u = np.array([])
    v = np.array([])
    w = np.array([])

    red = np.array([1, 0, 0])
    green = np.array([0, 1, 0])
    blue = np.array([0, 0, 1])
    colors = []

    for transformation in transformations:
        x = np.concatenate([x, [transformation[0, 3], transformation[0, 3], transformation[0, 3]]])
        y = np.concatenate([y, [transformation[1, 3], transformation[1, 3], transformation[1, 3]]])
        z = np.concatenate([z, [transformation[2, 3], transformation[2, 3], transformation[2, 3]]])

        u = np.concatenate([u, transformation[:3, 0]])
        v = np.concatenate([v, transformation[:3, 1]])
        w = np.concatenate([w, transformation[:3, 2]])

        colors.append(red)
        colors.append(green)
        colors.append(blue)

    [0, 0, 0, 1, 1, 1]

    q = ax.quiver(x, y, z, u, v, w, length=0.05, colors = colors, lw=1)

    for i in range(x.shape[0] - 3):
        plt.plot([x[i], x[i+3]], [y[i], y[i+3]], [z[i], z[i+3]], '--', color='black')

    plt.show()

def plot_joint_trajectory(q, qd, qdd, *ts):
    """
    Function to plot joint trajectories
    ...
    Parameters
    ---
    q   : Joint Position (Dof x m)
    qd  : Joint Velocity (Dof x m)
    qdd : Joint Acceleration (Dof x m)
    Returns
    ---
    None
    """

    # transpose matrixes if m > 6
    m = q.shape[1]
    n = q.shape[0]
    if n > 6:  # number of joints in ur
        q = np.transpose(q)
        qd = np.transpose(qd)
        qdd = np.transpose(qdd)

    m = q.shape[1]
    n = q.shape[0]

    # if there is a ts
    if len(ts[0]) > 1:  # If there are ts values
        timesteps = ts[0]
        tfromstart = ts[0]
        if len(timesteps) != m and len(timesteps) != n:
            timesteps = np.linspace(0, tfromstart[-1], num=m)
    else:
        # timesteps = np.linspace(0, tfromstart[-1], num=m)
        timesteps = np.linspace(0, 1, num=m)
    print("Plot timesteps \n", timesteps)



    fig, axis = plt.subplots(3)
    fig.suptitle("Joint Trajectories")

    # Joint Position Plot
    axis[0].set_title("Position")
    axis[0].set(xlabel="Time", ylabel="Position")
    for ij in range(n):
        axis[0].plot(timesteps, q[ij])

    # Joint Velocity Plot
    axis[1].set_title("Velocity")
    axis[1].set(xlabel="Time", ylabel="Velocity")
    for ij in range(n):
        axis[1].plot(timesteps, qd[ij])

    # Joint Acceleration Plot
    axis[2].set_title("Acceleration")
    axis[2].set(xlabel="Time", ylabel="Acceleration")
    for ij in range(n):
        axis[2].plot(timesteps, qdd[ij])

    # Legends
    legends = [f"Joint_{i + 1}" for i in range(n)]
    axis[0].legend(legends)
    axis[1].legend(legends)
    axis[2].legend(legends)

    fig.tight_layout()
    plt.show(block=False)
    plt.get_current_fig_manager().full_screen_toggle()  # toggle fullscreen mode
    plt.draw()
    print("---Plot graph finish---")
    plt.show()

def scatter3d(x, y, z, cs, colorsMap='jet'):
    # Scatter3d with ColorMap gradients
    # cs is the scalar map
    cm = plt.get_cmap(colorsMap)
    cNorm = matplotlib.colors.Normalize(vmin=min(cs), vmax=max(cs))
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)

    fig = plt.figure()
    ax3D = fig.add_subplot(111, projection='3d')
    p3d = ax3D.scatter(x, y, z, s=30, c=scalarMap.to_rgba(cs), marker='o')

    # if block:
    #     plt.show()
    # else:
    #     plt.show(block=False)
    return ax3D, plt

def scatter2d(x, y, cs, colorsMap='jet'):
    # Scatter2d with ColorMap gradients
    # cs is the scalar map
    cm = plt.get_cmap(colorsMap)
    cNorm = matplotlib.colors.Normalize(vmin=min(cs), vmax=max(cs))
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    p2d = ax.scatter(x, y, s=30, c=scalarMap.to_rgba(cs), marker='o')

    # if block:
    #     plt.show()
    # else:
    #     plt.show(block=False)

    return ax, plt


def mean_var_detection(arr, win=5, Z=1.5):
    # win = 5  # data set moving window
    n = len(arr)
    N = win
    r = Z  # From a normal distribution how many STD is it from the mean in window
    outa = np.zeros(n)
    indexes = []
    means = []
    variances = []

    fault1 = np.zeros(n)
    fault2 = np.zeros(n)
    z = []   # Decision based on value ui which follows a normal law N(0,1)
    j = 0  # counter of iterations when entering the ui Decision
    for i in np.arange(N, n):
        suba = arr[i - N: i]
        mni = np.mean(suba)
        vari = np.var(suba)

        print("i", i)
        print("N", N)

        if i == N:
            mn_prev = mni
        else:
            mn_prev = means[j - 1]

        # ui = mni - mn_prev
        # ui = ui / (np.sqrt(vari) / N)
        ui = arr[i] - mni
        sdi = np.sqrt(vari)
        zi = ui / sdi

        if abs(zi) > r:
            j += 1
            indexes.append(i)
            fault1[i] = 1
            fault2[i - int(np.ceil(N / 2))] = 1

        means.append(mni)
        variances.append(vari)
        z.append(zi)

    return means, variances, z, indexes, fault1, fault2

def vel_on_movement(pp, tt, th=0.1):
    # Detect when it is moving in a certain axis pp ( x y or z)
    #  ~Approximate velocities in the segments given the timestep between points dt
    # pp: 1xN points xx, or yy, or zz 
    # tt: 1xN time data points
    # th: threshold of speed (slope) for the discretization m/s
    # ---------
    # Return
    #   movement: 1xN discret 0 or 1 when in no movement/ movement
    #   indexes:   when this happen

    n = len(pp)
    indexes = []
    means = []
    vel = []

    movement = np.zeros(n)
    for i in range(n - 1):
        vi = (pp[i + 1] - pp[i - 1]) / (tt[i + 1] - tt[i - 1])
        vel.append(vi)
        if abs(vi) > th:
            indexes.append(i)
            movement[i] = 1
    return movement, indexes

def startsends_movement(movement):
    # movement: 0/1 array from vel_on_movement
    # ---------
    # Return
    #   start_indexes: indexes when it starts to move
    #   end_indexes: indexes when it stops
    n = len(movement)
    prev_state = movement[0]
    start_indexes = []
    end_indexes = []
    for i in range(1, n):
        act_state = movement[i]

        if act_state > prev_state:
            start_indexes.append(i - 1)
        elif act_state < prev_state:
            end_indexes.append(i - 1)

        prev_state = act_state

    return start_indexes, end_indexes
